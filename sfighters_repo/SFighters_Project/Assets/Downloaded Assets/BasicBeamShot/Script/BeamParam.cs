﻿using UnityEngine;
using System.Collections;

public class BeamParam : MonoBehaviour {
	
	public Color BeamColor = Color.white;
	public float AnimationSpd = 0.1f;
	public float Scale = 1.0f;
	public float MaxLength = 32.0f;
	public bool bEnd = false;
	public bool bGero = false;

	public void SetBeamParam(BeamParam param)
	{
		this.BeamColor = param.BeamColor;
		this.AnimationSpd = param.AnimationSpd;
		this.Scale = param.Scale;
		this.MaxLength = param.MaxLength;
	}

	void Start () {
		BeamParam param = this.transform.root.gameObject.GetComponent<BeamParam>();

		if(param != null)
		{
            //if (gameObject.GetComponent<Bullet>().FriendlyParent.transform.parent.tag == "P1")
            //{
            //    //this.BeamColor = Color.cyan;
            //}
            //if (gameObject.GetComponent<Bullet>().FriendlyParent.transform.parent.tag == "P2")
            //{
            //    this.BeamColor = Color.red;
            //}
            //if (gameObject.GetComponent<Bullet>().FriendlyParent.transform.parent.tag == "P3")
            //{
            //    this.BeamColor = Color.green;
            //}
            //if (gameObject.GetComponent<Bullet>().FriendlyParent.transform.parent.tag == "P4")
            //{
            //    this.BeamColor = Color.yellow;
            //}
            //this.BeamColor = param.BeamColor;
			//this.AnimationSpd = param.AnimationSpd;
			//this.Scale = param.Scale;
			//this.MaxLength = param.MaxLength;
		}

	}
}
