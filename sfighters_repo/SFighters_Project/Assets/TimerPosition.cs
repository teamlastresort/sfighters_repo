﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerPosition : MonoBehaviour {
    public Vector3 position;
    public RectTransform bgPosition;
    public RectTransform timerPosition;
    
	// Use this for initialization
	void Start () {
        GameObject temp = GameObject.FindGameObjectWithTag("PlayerManager");

        if (temp.GetComponent<PlayerManager>() != null)
        {
            if (temp.GetComponent<PlayerManager>().playerNum == 1)
            {
                bgPosition.localPosition = position;
                timerPosition.localPosition = position;
            }

        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
