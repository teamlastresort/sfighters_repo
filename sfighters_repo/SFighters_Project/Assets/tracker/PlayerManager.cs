﻿using UnityEngine;
using System.Collections.Generic;


public class PlayerManager : MonoBehaviour {
    public int playerNum = 0;


    public List<GameObject> players = new List<GameObject>();

    public int PlayerCount
    {
        get { return players.Count; }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// players register themselves
    /// </summary>
    /// <param name="player"></param>
    public void RegisterPlayer(GameObject player)
    {
        players.Add(player);
    }

    public GameObject GetPlayer(int playerNum)
    {
        foreach(GameObject p in players)
        {
            Debug.Log(playerNum);
            if (p.tag == "P" + playerNum.ToString())
                return p;
        }

        return null;
    }
}
