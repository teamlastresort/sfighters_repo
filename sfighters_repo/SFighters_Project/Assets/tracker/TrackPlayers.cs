﻿using UnityEngine;
using System.Collections;

public class TrackPlayers : MonoBehaviour {


    public GameObject trackerPrefab;

    private Transform[] players;

    //trackers we create
    private Transform[] trackers;
    private int player;
    private Camera shipCamera;
    private PlayerManager playerManager;
    // Use this for initialization
    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        shipCamera = transform.parent.gameObject.GetComponentInChildren<Camera>();

        //reset tracker transforms
        transform.localScale = Vector3.one;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        //grab tag from parent
        transform.tag = transform.parent.tag;

        //work out who we are
        for (int p = 1; p <= 4; p++)
        {
            if (transform.CompareTag("P" + p.ToString()))
            {
                player = p;
                break;
            }
        }

        int mask =  1 << (player + 7);
        
        //set our camera's layers to only view the layer based on our tag
        shipCamera.cullingMask = mask | shipCamera.cullingMask;

    }
	

    private void FindPlayers()
    {
      
        players = new Transform[playerManager.PlayerCount - 1];

        int playerCount = 0;

        //grab players
        for (int p = 1; p <= playerManager.PlayerCount; p++)
        {
            if (p == player)
                continue;

            players[playerCount] = playerManager.GetPlayer(p).transform;
            //players[playerCount] = GameObject.FindGameObjectWithTag("P" + p.ToString()).transform;
            playerCount++;
        }

        int currPlayer = 0;

        GameObject parentObject = new GameObject("P" + player.ToString() + "Trackers");
        trackers = new Transform[players.Length];

        //create trackers for each player
        foreach (Transform p in players)
        {
            trackers[currPlayer] = ((GameObject)Instantiate(trackerPrefab, p.transform.position, Quaternion.identity)).transform;
            trackers[currPlayer].gameObject.layer = LayerMask.NameToLayer("P" + player.ToString());
            trackers[currPlayer].transform.SetParent(parentObject.transform);
            currPlayer++;
        }


    }

    private bool playersFound = false;
	// Update is called once per frame
	void Update () {

        //wait until all players registered in start screen have been added
        //to player manager.
        if (playersFound == false)
        {
            if (playerManager.PlayerCount == PlayerPrefs.GetInt("PlayerNum") + 1 )
            {
                FindPlayers();
                playersFound = true;
            }
            else
            {
                return;
            }
        }


        for(int i = 0; i < trackers.Length; i++)
        {
            trackers[i].transform.position = players[i].transform.position;
            trackers[i].transform.rotation = Quaternion.LookRotation(shipCamera.transform.forward * 1.0f);

            //scale based on distance
            trackers[i].transform.localScale = Vector3.Distance(transform.position, players[i].transform.position) * Vector3.one * 0.5f;


        }

	}
}
