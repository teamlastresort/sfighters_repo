﻿using UnityEngine;
using System.Collections;

public class LoadShipChoiceReference : MonoBehaviour {

    //ADD THIS LINE
    private PlayerManager playerManager;

    public int playerNum;
    public GameObject playerPrefab;
    public bool generatePlayer = false;

	// Use this for initialization
	void Awake () {

        //ADD THIS LINE
        playerManager = FindObjectOfType<PlayerManager>();

       
    }

    // Update is called once per frame
    void Update () {
	
        
        if(generatePlayer)
        {
            GameObject ship = Instantiate(playerPrefab, transform.position, Quaternion.identity) as GameObject;

            ship.name = "Player" + playerNum.ToString();
            ship.tag = "P" + playerNum.ToString();
            //whem created

            //ADD THIS LINE WHERE YOU CREATE THE SHIP
            playerManager.RegisterPlayer(ship);

            generatePlayer = false;
        }

	}
}
