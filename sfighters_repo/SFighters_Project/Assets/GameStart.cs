﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InControl;
public class GameStart : MonoBehaviour
{
    public float startDelay;
    public GameObject instructionCanvas;
    private int playerNum;
    private bool firstRun = true;

    private bool isActive = true;

	// Use this for initialization
	void Start ()
    {
        Time.timeScale = 0.0001f;

  	}

    void Update()
    {
        //Debug.Log(InputManager.Devices.Count);
        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (InputManager.Devices[playerNum] == null)
        {
            // If no controller exists for this cube, just make it translucent.
            Debug.Log("no ctrl");

        }
        else
        {
            //  Debug.Log("Controller is okay");
            UpdateStart(InputManager.Devices[playerNum]);
        }
    }

    // Update is called once per frame
    void UpdateStart(InputDevice inputDevice)
    {
        if(firstRun)
        {
            firstRun = false;
            return;
        }

        
//        startDelay -= Time.unscaledDeltaTime;
//
//
//        if (startDelay <= 0)
//        {
//            Time.timeScale = 1;
//            instructionCanvas.gameObject.SetActive(false);
//            isActive = false;
//            Destroy(this);
//        }

        if(isActive == true)
        {
            if(inputDevice.Action1.WasPressed)
            {
                Time.timeScale = 1;
                instructionCanvas.gameObject.SetActive(false);
                isActive = false;
                Destroy(this);

            }
        }

        


    }

 



}
