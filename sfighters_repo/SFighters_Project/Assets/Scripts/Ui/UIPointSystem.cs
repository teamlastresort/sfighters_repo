﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIPointSystem : MonoBehaviour {
    public Text Score;
    public Text BoundsDeathTimer;
    public Image Health;
    public Image Boost;
    public Image Heat;
    private string powerStore;
    public Image PowerUp;


    public Sprite MissileIcon;
    public Sprite EMPIcon;
    public Sprite ShieldIcon;
    public Sprite NoPowerUp;
    public GameObject AButton;
    //public Text NumOfPlayers;
    //public Text BoostFuel;
    public GameObject Player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Health.fillAmount = Player.GetComponent<ShipStats>().ReturnHealth() / Player.GetComponent<ShipStats>().MaxHealth;
        Score.text = Player.GetComponent<ShipStats>().Score.ToString();

        Boost.fillAmount= Player.GetComponent<ShipMovement>().BoostFuel / Player.GetComponent<ShipMovement>().BoostFuelMax;
        Heat.fillAmount= Player.GetComponent<Shoot>().CurHeat / Player.GetComponent<Shoot>().MaxHeat;


        powerStore = Player.GetComponent<Inventory>().CurrentPower();

        if(powerStore == "Missile")
        {
            PowerUp.sprite = MissileIcon;
            AButton.SetActive(true);
        }
        else if (powerStore == "EMP")
        {
            PowerUp.sprite = EMPIcon;
            AButton.SetActive(true);
        }
        else if (powerStore == "Shield")
        {
            PowerUp.sprite = ShieldIcon;
            AButton.SetActive(true);
        }
        else if(powerStore == "No power-up")
        {
            PowerUp.sprite = NoPowerUp;
            AButton.SetActive(false);
        }
        //NumOfPlayers.text = Player.GetComponent<NumberOfPlayers>().numberOfPlayers.ToString();


    }
}
