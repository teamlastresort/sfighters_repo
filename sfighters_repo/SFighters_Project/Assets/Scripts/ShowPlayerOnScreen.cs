﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowPlayerOnScreen : MonoBehaviour {

    public Transform cameraTarget1;
    public Transform cameraTarget2;
    public Transform cameraTarget3;

    public Camera ScreenCamera;

    public Transform imageTransform1;
    public Transform imageTransform2;
    public Transform imageTransform3;


    
    // Use this for initialization
    void Start ()
    {

       if(this.tag == "P1")
       {
            cameraTarget1 = GameObject.FindGameObjectWithTag("P2").gameObject.transform.GetChild(0).transform;
           cameraTarget2 = GameObject.FindGameObjectWithTag("P3").gameObject.transform.GetChild(0).transform; 
           cameraTarget3 = GameObject.FindGameObjectWithTag("P4").gameObject.transform.GetChild(0).transform; 
       }
       if (this.tag == "P2")
       {
           cameraTarget1 = GameObject.FindGameObjectWithTag("P1").transform.GetChild(0).transform;
           cameraTarget2 = GameObject.FindGameObjectWithTag("P3").transform.GetChild(0).transform;
           cameraTarget3 = GameObject.FindGameObjectWithTag("P4").transform.GetChild(0).transform; 
       }
       if (this.tag == "P3")
       {
           cameraTarget1 = GameObject.FindGameObjectWithTag("P1").transform.GetChild(0).transform;
           cameraTarget2 = GameObject.FindGameObjectWithTag("P2").transform.GetChild(0).transform;
           cameraTarget3 = GameObject.FindGameObjectWithTag("P4").transform.GetChild(0).transform; 
       }
       if (this.tag == "P4")
       {
           cameraTarget1 = GameObject.FindGameObjectWithTag("P1").transform.GetChild(0).transform;
           cameraTarget2 = GameObject.FindGameObjectWithTag("P2").transform.GetChild(0).transform;
           cameraTarget3 = GameObject.FindGameObjectWithTag("P3").transform.GetChild(0).transform; 
       }


    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 screenPos = ScreenCamera.WorldToViewportPoint(cameraTarget1.position);
        Vector3 screenPos2 = ScreenCamera.WorldToViewportPoint(cameraTarget2.position);
        Vector3 screenPos3 = ScreenCamera.WorldToViewportPoint(cameraTarget3.position);





        float dot = Vector3.Dot(transform.forward, (cameraTarget1.transform.position - transform.position).normalized);
        float dot2 = Vector3.Dot(transform.forward, (cameraTarget2.transform.position - transform.position).normalized);
        float dot3 = Vector3.Dot(transform.forward, (cameraTarget1.transform.position - transform.position).normalized);

        if (dot <= 0.0f)
        {
            imageTransform1.gameObject.SetActive(false);
            //swap this to different arrow instead
        }
        else
        {
            imageTransform1.gameObject.SetActive(true);
        }

        if (dot2 <= 0.0f)
        {
            imageTransform2.gameObject.SetActive(false);
            //swap this to different arrow instead
        }
        else
        {
            imageTransform2.gameObject.SetActive(true);
        }
        if (dot3 <= 0.0f)
        {
            imageTransform3.gameObject.SetActive(false);
            //swap this to different arrow instead
        }
        else
        {
            imageTransform3.gameObject.SetActive(true);
        }

        //screen pos is normalized 0,0 is bottom-right.
        float width = ScreenCamera.pixelWidth;
        float height = ScreenCamera.pixelHeight;

        imageTransform1.GetComponent<RectTransform>().anchoredPosition = new Vector3(screenPos.x * width - imageTransform1.GetComponent<RectTransform>().sizeDelta.x / 2, screenPos.y * height - imageTransform1.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
        imageTransform2.GetComponent<RectTransform>().anchoredPosition = new Vector3(screenPos2.x * width - imageTransform2.GetComponent<RectTransform>().sizeDelta.x / 2, screenPos2.y * height - imageTransform2.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
        imageTransform3.GetComponent<RectTransform>().anchoredPosition = new Vector3(screenPos3.x * width - imageTransform3.GetComponent<RectTransform>().sizeDelta.x / 2, screenPos3.y * height - imageTransform3.GetComponent<RectTransform>().sizeDelta.y / 2, 0);

        imageTransform1.GetComponent<RectTransform>().localScale = Vector3.one;
        imageTransform2.GetComponent<RectTransform>().localScale = Vector3.one;
        imageTransform3.GetComponent<RectTransform>().localScale = Vector3.one;

        // imageTransform.position = screenPos;
    }
}
