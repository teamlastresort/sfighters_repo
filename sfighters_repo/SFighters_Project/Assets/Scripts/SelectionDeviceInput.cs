﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using InControl;


public class SelectionDeviceInput : MonoBehaviour {

    private RevampedInput revampedInput;
    public int playerIndex = 0;
    //current choice
    private int CurButton = 0;

    //possible choices
    private int MaxButtons;

    private bool isReleased = true;

    private bool RFirstStick = false;
    private bool RStickHeld = false;

    private bool UFirstStick = false;
    private bool UStickHeld = false;
    public float holdChangetimer;
    /// <summary>
    /// time between holding the stick for quick scroll through and first knock of stick
    /// </summary>
    public float ScrollTimer;
    /// <summary>
    /// scroll through timer, buttons per second
    /// </summary>
    private float holdTimer;

    private int prevIndex = 0;
    // Use this for initialization
    void Start()
    {
        MaxButtons = 4;
        revampedInput = FindObjectOfType<RevampedInput>();
    }

    // Update is called once per frame

    void Update()
    {

        var inputDevice = (InputManager.Devices.Count > 0) ? InputManager.Devices[playerIndex] : null;
        if (inputDevice == null)
        {
            // If no controller exists for this cube, just make it translucent.
        }
        else
        {
            UpdateScroll(inputDevice);
        }
    }
    void UpdateScroll(InputDevice inputDevice)
    {
        if (!revampedInput.selectingShip[playerIndex])
            return;

        if (inputDevice.LeftStick.Left.Value > .85)
        {
            if (!UFirstStick)
            {
                UFirstStick = true;
                if (!RFirstStick)
                {
                    holdTimer = holdChangetimer;

                    CurButton--;

                }
            }
            holdTimer -= Time.deltaTime;

            if (holdTimer <= 0)
            {

                

                CurButton--;
                if (UFirstStick == true)
                {
                    UStickHeld = true;
                }
                if (UStickHeld == true && holdTimer <= 0)
                {
                    holdTimer = ScrollTimer;
                }
            }

            if (CurButton < 0)
            {
                CurButton = MaxButtons - 1;
            }
        }
        else
        {
            UFirstStick = false;
            UStickHeld = false;

        }
        if (inputDevice.LeftStick.Right.Value > .85)
        {

            if (!RFirstStick)
            {
                RFirstStick = true;
                if (!UFirstStick)
                {
                    holdTimer = holdChangetimer;

                    CurButton++;
                }
            }
            holdTimer -= Time.deltaTime;

            if (holdTimer <= 0)
            {
                CurButton++;

                isReleased = false;
                if (RFirstStick == true)
                {
                    RStickHeld = true;
                }
                if (RStickHeld == true && holdTimer <= 0)
                {
                    holdTimer = ScrollTimer;
                }
            }

            if (CurButton >= MaxButtons)
            {
                CurButton = 0;
            }


        }
        else
        {
            RFirstStick = false;
            RStickHeld = false;

        }

        //communicate with revamped input and set shipIndex to currIndex
        if (prevIndex != CurButton)
            revampedInput.UpdateIndex(playerIndex, CurButton);

        prevIndex = CurButton;
    }
}


