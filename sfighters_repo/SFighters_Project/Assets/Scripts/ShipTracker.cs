﻿using UnityEngine;
using System.Collections;

public class ShipTracker : MonoBehaviour
{
    [Tooltip("This is the prefab for the Tracker")]
    public GameObject trackerPrefab;

    [Tooltip("This is the player parents, Drag in all except the current camera's parent.")]
    public Transform[] players;

    //trackers we create
    private Transform[] trackers;

    [Tooltip("This is the current players number")]
    public int player;

    // Use this for initialization
    void Start()
    {
        int currPlayer = 0;

        trackers = new Transform[players.Length];

        //create trackers for each player
        foreach (Transform p in players)
        {
            trackers[currPlayer] = ((GameObject)Instantiate(trackerPrefab, p.transform.position, Quaternion.identity)).transform;
            trackers[currPlayer].gameObject.layer = LayerMask.NameToLayer("P" + player.ToString());
            currPlayer++;
        }

    }

    // Update is called once per frame
    void Update()
    {


        for (int i = 0; i < trackers.Length; i++)
        {
            trackers[i].transform.position = players[i].transform.position;
            trackers[i].transform.rotation = Quaternion.LookRotation(GetComponentInChildren<Camera>().transform.forward * 1.0f);

            //scale based on distance
            trackers[i].transform.localScale = Vector3.Distance(transform.position, players[i].transform.position) * Vector3.one * 0.5f;


        }

    }
}
