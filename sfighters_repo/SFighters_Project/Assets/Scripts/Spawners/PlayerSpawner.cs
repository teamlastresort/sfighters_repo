﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {
    public float RespawnTimer;
    public GameObject PlayerToSpawn;
    public  bool Respawning = false;
    public float MaxPlayerHealth;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Respawning)
        {
            if (RespawnTimer <= 0)
            {


                PlayerToSpawn.transform.position = transform.position;
                PlayerToSpawn.transform.rotation = transform.rotation;

                PlayerToSpawn.GetComponent<ShipStats>().enabled = true;
                PlayerToSpawn.GetComponent<ShipStats>().Health = MaxPlayerHealth;
                PlayerToSpawn.SetActive(true);
                PlayerToSpawn.GetComponent<ShipMovement>().Reset();
                PlayerToSpawn = null;
                Respawning = false;
            }
            RespawnTimer -= Time.deltaTime;
        }
	}
    public void SetSpawner(GameObject a_Player)
    {
        if (PlayerToSpawn == null)
        {
            PlayerToSpawn = a_Player;
            RespawnTimer = 3;
            Respawning = true;
        }
    }
}
