﻿using UnityEngine;
using System.Collections;

public class PickUpSpawner : MonoBehaviour {
    public GameObject PickUp;
    public GameObject Target;
    public float RespawnTimer;
    public float MaxRespawnTime;
    public bool Respawning = false;
    private bool isTarget;
    // Use this for initialization
	void Start () {
        if (GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<PlayerManager>().PlayerCount == 1)
        {
            PickUp.SetActive(false);
            isTarget = true;
        }
        else
        {
            Target.SetActive(false);
            isTarget = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Respawning)
        {
            if (RespawnTimer <= 0)
            {
                if (!isTarget)
                {

                    PickUp.SetActive(true);
                    if (PickUp.GetComponent<PickUp>() == true)
                        PickUp.GetComponent<PickUp>().Spawner = gameObject;
                    else if (PickUp.GetComponent<Target>() == true)
                        PickUp.GetComponent<Target>().Spawner = gameObject;
                    Instantiate(PickUp);
                    Respawning = false;
                }
                else
                {
                    Target.SetActive(true);
                    if (Target.GetComponent<PickUp>() == true)
                        Target.GetComponent<PickUp>().Spawner = gameObject;
                    else if (Target.GetComponent<Target>() == true)
                        Target.GetComponent<Target>().Spawner = gameObject;
                    Instantiate(Target);
                    Respawning = false;

                }

            }

            RespawnTimer -= Time.deltaTime;
        }
	}


    public void StartRespawn()
    {
        Respawning = true;
        RespawnTimer = MaxRespawnTime;

    }
}
