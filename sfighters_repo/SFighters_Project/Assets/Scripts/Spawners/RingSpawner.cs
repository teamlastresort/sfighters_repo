﻿using UnityEngine;
using System.Collections;

public class RingSpawner : MonoBehaviour {
    public GameObject Ring;
    private bool Respawning;
    private float RespawnTimer;
    public float MaxRespawnTime;
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Respawning)
        {
            if (RespawnTimer <= 0)
            {
                Ring.SetActive(true);
                Respawning = false;

            }

            RespawnTimer -= Time.deltaTime;
        }
    }


    public void StartRespawn()
    {
        Respawning = true;
        RespawnTimer = MaxRespawnTime;

    }

}
