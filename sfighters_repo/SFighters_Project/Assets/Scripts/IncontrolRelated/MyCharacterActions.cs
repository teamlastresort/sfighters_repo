﻿using UnityEngine;
using System.Collections;
using InControl;
public class MyCharacterActions : PlayerActionSet {
        public PlayerAction XMovement;
        public PlayerAction YMovement;
        public PlayerAction Shoot;
        public PlayerAction BarrelRoll;
        public PlayerAction Boost;
        public PlayerAction UsePower;
        public PlayerAction Throttle;
        public PlayerAction Rotate;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public MyCharacterActions()
    {
         XMovement = CreatePlayerAction("XMovement");
         YMovement = CreatePlayerAction("YMovement");
             Shoot = CreatePlayerAction("Shoot");
        BarrelRoll = CreatePlayerAction("BarrelRoll");
             Boost = CreatePlayerAction("Boost");
          UsePower = CreatePlayerAction("UsePower");
          Throttle = CreatePlayerAction("Throttle");
            Rotate = CreatePlayerAction("Rotate");
        
    }
}
