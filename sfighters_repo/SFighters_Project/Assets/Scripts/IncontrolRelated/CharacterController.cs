﻿using UnityEngine;
using System.Collections;
using InControl;
public class CharacterController : MonoBehaviour {

    MyCharacterActions  CharacterActions;
	// Use this for initialization
	void Start () {
        CharacterActions = new MyCharacterActions();

        CharacterActions.XMovement.AddDefaultBinding(InputControlType.LeftStickRight);
        CharacterActions.XMovement.AddDefaultBinding(InputControlType.LeftStickLeft);

        CharacterActions.YMovement.AddDefaultBinding(InputControlType.LeftStickUp);
        CharacterActions.YMovement.AddDefaultBinding(InputControlType.LeftStickDown);

        CharacterActions.Boost.AddDefaultBinding(InputControlType.Action2);

        CharacterActions.Shoot.AddDefaultBinding(InputControlType.RightTrigger);

        CharacterActions.Throttle.AddDefaultBinding(InputControlType.LeftTrigger);

        CharacterActions.BarrelRoll.AddDefaultBinding(InputControlType.Action3);

        CharacterActions.UsePower.AddDefaultBinding(InputControlType.Action1);

        CharacterActions.Rotate.AddDefaultBinding(InputControlType.LeftBumper);
        CharacterActions.Rotate.AddDefaultBinding(InputControlType.RightBumper);


    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(CharacterActions.XMovement.WasRepeated);
        Debug.Log(CharacterActions.YMovement.WasRepeated);
        Debug.Log(CharacterActions.Boost.WasRepeated);
        Debug.Log(CharacterActions.Shoot.WasRepeated);
        Debug.Log(CharacterActions.Throttle.WasRepeated);
        Debug.Log(CharacterActions.BarrelRoll.WasRepeated);
        Debug.Log(CharacterActions.UsePower.WasRepeated);
        Debug.Log(CharacterActions.Rotate.WasRepeated);
    }
    private void MoveHorizontal()
    {

    }
    private void MoveVertical()
    {

    }
    private void Shoot()
    {

    }



}
