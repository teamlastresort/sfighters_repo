﻿using UnityEngine;
using System.Collections;

public class LoadShipChoice : MonoBehaviour {

    public bool created = false;


    public GameObject[] shipModels;
    public Transform shipMesh;
    public Rect CamRect;

    
    private PlayerManager playerManager;
    private Transform targetHighlight;
    private GameObject Ship;

    // Use this for initialization
    void Awake () {
        playerManager = FindObjectOfType<PlayerManager>();
        
        
        if (created)
            return;
        if (playerManager.playerNum <= PlayerPrefs.GetInt("PlayerNum"))
        {


        int playerChoice = PlayerPrefs.GetInt("P" + (playerManager.playerNum + 1) + "Choice");
        //if (playerChoice == 0)
        //    return;


            //shipMesh.sharedMesh = shipModels[playerChoice].GetComponentInChildren<MeshFilter>().sharedMesh;
            Ship = shipModels[playerChoice];
            
            Ship = Instantiate(shipModels[playerChoice], shipMesh.transform, false) as GameObject;

            Ship.tag = "P" + (playerManager.playerNum + 1);


            playerManager.RegisterPlayer(Ship);



            Ship.transform.localPosition = Vector3.zero;

            created = true;
            if (playerManager.playerNum == 0)
            {
                if (PlayerPrefs.GetInt("PlayerNum") == 1)
                {
                    CamRect.width = 1;
                }
                CamRect.x = 0;
                CamRect.y = 0.5f;

                if (playerManager.players.Count == 1)
                {
                    CamRect.width = 2;
                    CamRect.height = 2;
                    CamRect.x = 0;
                    CamRect.y = 0;
                }
            }
            if (playerManager.playerNum == 1)
            {
                if (PlayerPrefs.GetInt("PlayerNum") == 1)
                {

                    CamRect.x = 0.0f;
                    CamRect.y = 0.0f;
                    CamRect.width = 1;
                }
                else
                {
                    CamRect.x = 0.5f;
                    CamRect.y = 0.5f;
                    CamRect.width = .5f;
                }

            }
            if (playerManager.playerNum == 2)
            {
                CamRect.x = 0f;
                CamRect.y = 0f;
            }
            if (playerManager.playerNum == 3)
            {
                CamRect.x = 0.5f;
                CamRect.y = 0f;
            }
            playerManager.playerNum++;
        }


    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public int ReturnPlayerNum()
    {
        return playerManager.playerNum;
    }
}
