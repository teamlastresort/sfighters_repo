﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public bool Abutton;
    private int playerNum;
    public string BackScene;
    // Use this for initialization
    void Start ()
    {
        //Sets playernum.
        playerNum = 2 - 1;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(InputManager.Devices.Count);
        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (InputManager.Devices[playerNum] == null)
        {
            // If no controller exists for this cube, just make it translucent.
            Debug.Log("no ctrl");

        }
        else
        {
            Debug.Log("Controller is okay");
            UpdateEnd(InputManager.Devices[playerNum]);
        }
    }
    void UpdateEnd(InputDevice inputDevice)
    {
        if (Abutton)
        {
            if(inputDevice.Action1.WasPressed)
            {
                SceneManager.LoadScene(BackScene);
            }

        }
        else
        {
            if (inputDevice.Action2.WasPressed)
            {
                SceneManager.LoadScene(BackScene);
            }
        }
    }
}
