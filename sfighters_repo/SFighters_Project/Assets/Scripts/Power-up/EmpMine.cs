﻿using UnityEngine;
using System.Collections;

public class EmpMine : MonoBehaviour {

    private bool IsActive;
    private bool IsReady;
    public float DropTimer;
    public float EMPBlastDuration;
    public float BlastGrowth;
    public float MaxBlastRadius;

    public GameObject empRing;

    private AudioManager audioManager;
    public GameObject audioSourceExplosion;
    public float ExplosionVolume;
    private Animator animator;
    
    // Use this for initialization
    void Start ()
    {
        animator = GetComponent<Animator>();
        animator.StopPlayback();
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        audioSourceExplosion.GetComponent<AudioSource>().clip = audioManager.EMPBlast;
        DropTimer = 1.0f;
        IsActive = false;
        IsReady = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (IsActive == true)
        //{
        //    if (GetComponent<SphereCollider>().radius <= MaxBlastRadius)
        //    {

        //        GetComponent<SphereCollider>().radius += BlastGrowth * Time.deltaTime;
        //    }
        //}
        if (IsReady == false)
        {
            //decrement timer
            DropTimer -= Time.deltaTime;
            if (DropTimer <= 0)

                IsReady = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (IsReady == false)
            return;

        animator.SetTrigger("Activated");

        Destroy(gameObject, EMPBlastDuration);
        if (other.gameObject.tag == "Player")
        {
            audioSourceExplosion.GetComponent<AudioSource>().volume = ExplosionVolume;
            audioSourceExplosion.transform.position = transform.position;
            audioSourceExplosion.GetComponent<AudioSource>().Play();
            Instantiate(audioSourceExplosion);
            
            IsActive = true;
            Instantiate(audioSourceExplosion);
           
            if (!(other.GetComponentInParent<ShipStats>().shielded))
            {
            other.GetComponentInParent<ShipMovement>().LockControls();

            }
            Instantiate(empRing, transform.position, transform.rotation);
            
        }
        if (other.gameObject.GetComponent<MissileMovement>() != null)
        {
            audioSourceExplosion.GetComponent<AudioSource>().volume = ExplosionVolume;
            audioSourceExplosion.transform.position = transform.position;
            audioSourceExplosion.GetComponent<AudioSource>().Play();
            Instantiate(audioSourceExplosion);
            Destroy(other);
        }
        if (other.GetComponent<Bullet>() != null)
        {
            audioSourceExplosion.GetComponent<AudioSource>().volume = ExplosionVolume;
            audioSourceExplosion.transform.position = transform.position;
            audioSourceExplosion.GetComponent<AudioSource>().Play(); Destroy(gameObject, EMPBlastDuration);

            Instantiate(empRing, transform.position, transform.rotation);
        }
    }



    void OnTriggerStay(Collider other)
    {
        if(IsActive == true)
        {//change to timer
            if(other.gameObject.tag == "Player")
            {
                if (!(other.GetComponentInParent<ShipStats>().shielded))
                {
                other.GetComponentInParent<ShipMovement>().LockControls();

                }
                //Instantiate(empRing, transform.position, transform.rotation);
            }
        }
    }




}
