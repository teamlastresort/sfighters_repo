﻿using UnityEngine;
using System.Collections;

public class EmpRadius : MonoBehaviour
{

    //public int startSize = 1;
    //public int minSize = 1;
    //public int maxSize = 50;

    //public float expansionSpeed = 10.0f;

    private float _currentScale = InitScale;
    //the max scale
    private const float TargetScale = 50.0f;
    //initial scale
    private const float InitScale = 0.1f;

    //amount of frames to hit max scale.
    public const int FrameCount = 100;

    //increases the speed of scaling
    public const float AnimationTimeSeconds = 0.5f;

    
    //deltatime
    private const float _deltaTime = AnimationTimeSeconds/FrameCount;

    //increases the scale
    private float _dx = (TargetScale - InitScale) / FrameCount;

    //Boolean to determine if we are still scaling up.
    private bool _upScale = true;

    
    private IEnumerator Expand()
    {
        yield return new WaitForSeconds(2);
        while (true)
        {
            while(_upScale)
            {
                _currentScale += _dx;
                if (_currentScale > TargetScale)
                {
                    _upScale = false;
                    _currentScale = TargetScale;
                }
                transform.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);

                if(_currentScale == TargetScale)
                {
                    Destroy(gameObject);
                }
            }
            while (!_upScale)
            {
                _currentScale -= _dx;  
                if(_currentScale < InitScale)
                {
                    _upScale = true;
                    _currentScale = InitScale;
                }
                transform.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);
                if (_currentScale == TargetScale)
                {
                    Destroy(gameObject);
                }
            }
        }
    }




    // Use this for initialization
    void Start ()
    {
        StartCoroutine(Expand());
    }




    // Update is called once per frame
    void Update ()
    {

	}
}
