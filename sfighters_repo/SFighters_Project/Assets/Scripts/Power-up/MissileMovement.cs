﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissileMovement : MonoBehaviour
{
    private float closestDist;
    public float Damage;
    public float TurnSpeed;
    public float MaxDist;
    public float MoveSpeed;
    private Vector3 AdjustedDirection;
    private Vector3 Direction;
    private Vector3 fwd;

    public GameObject Target;
    public GameObject Friendly;
    private List<GameObject> PlayerList = new List<GameObject>();
    private bool TargetFound = false;
    private RaycastHit rayhit;

    private AudioManager audioManager;
    public AudioSource audioSourceMissile;
    public float MissileVolume;
    private bool armed = false;
    public float armtimer;
    public float MoveSpeedIncrease;
    public float TurnSpeedIncrease;
    // Use this for initialization
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        audioSourceMissile.volume = MissileVolume;
        audioSourceMissile.clip = audioManager.MissileTravel;
        audioSourceMissile.loop = true;
        audioSourceMissile.Play();

        //audio set up.

        closestDist = float.MaxValue;
        for (int i = 0; i < 4; i++)
        {
            if (GameObject.FindGameObjectWithTag("P" + (i+1)) != null)
                PlayerList.Add(GameObject.FindGameObjectWithTag("P" + (i + 1)));
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveSpeed += MoveSpeedIncrease * Time.deltaTime;
        if(armed)
        {

            if (!TargetFound)
            {
                for (int i = 0; i < PlayerList.Count; i++)
                {
                    if (PlayerList[i] != null)
                    {
                        if (PlayerList[i].tag != Friendly.tag)
                        {
                            if (PlayerList[i].transform.position.magnitude - transform.position.magnitude < closestDist && PlayerList[i].transform.position.magnitude - transform.position.magnitude < MaxDist)
                            {
                                closestDist = PlayerList[i].transform.position.magnitude - transform.position.magnitude;
                                Target = PlayerList[i].GetComponent<ShipMovement>().ChildMesh;
                                TargetFound = true;
                            }
                        }
                    }
                }
            }
            else
            {
                TurnSpeed += TurnSpeedIncrease * Time.deltaTime;
                Direction = Target.transform.position - transform.position;
                Direction.Normalize();
                AdjustedDirection = Vector3.RotateTowards(transform.forward, Direction, TurnSpeed, 0.0F);
                Quaternion targetRotation = Quaternion.LookRotation(AdjustedDirection.normalized);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * TurnSpeed);
            }
        }
        else
        {
            armtimer -= Time.deltaTime;
            if (armtimer <= 0)
                armed = true;
        }

        transform.position += transform.forward * MoveSpeed * Time.deltaTime;

        fwd = transform.TransformDirection(transform.forward);

        //if (Physics.Raycast(transform.position, fwd, out rayhit, MoveSpeed * Time.deltaTime))
        //{
        //    if (rayhit.transform.gameObject.tag == "Shield")
        //    {
        //        Destroy(gameObject);
        //        return;
        //    }
        //    if (rayhit.transform.gameObject.tag == "Player")
        //    {
        //        Debug.Log("yo bro");
        //        if (!rayhit.transform.gameObject.GetComponentInParent<ShipStats>().shielded)
        //            rayhit.transform.gameObject.GetComponentInParent<ShipStats>().HealthDamage(Damage);
        //        Destroy(gameObject);
        //        return;
        //    }
        //
        //    if (rayhit.transform.gameObject.tag == "point")
        //    {
        //        return;
        //    }
        //    if (rayhit.transform.gameObject.tag == "Bullet")
        //    {
        //        if (!rayhit.transform.gameObject.GetComponent<Bullet>().FriendlyParent != gameObject.GetComponent<Bullet>().FriendlyParent)
        //            Destroy(gameObject);
        //    }
        //    if (rayhit.transform.gameObject.tag != "no tag")
        //    {
        //        Destroy(gameObject);
        //    }
        //    if (rayhit.transform.gameObject.tag == "Collidables")
        //    {
        //        Destroy(gameObject);
        //
        //    }
        //}

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
            if (!(other.GetComponentInParent<ShipStats>().shielded))
            {
            other.GetComponentInParent<ShipStats>().HealthDamage(Damage);
                if (other.GetComponentInParent<ShipStats>().Health <= 0 && other.GetComponentInParent<ShipMovement>().isDead != true)
                {
                    Friendly.GetComponentInParent<ShipStats>().Score += 1;

                }
            }

        }
        if (other.tag == "Collidables")
        {
        Destroy(gameObject);

        }
    }
}
