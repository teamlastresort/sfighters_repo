﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {
    private AudioManager audioManager;
    public AudioSource audioSourceShield;
    public float ShieldVolume;
    public float LifeTime;
	// Use this for initialization
	void Start () {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        //audioSourceShield.clip = audioManager.ShieldActivate;
        audioSourceShield.volume = ShieldVolume;
            audioSourceShield.Play();
    }

    // Update is called once per frame
    void Update () {

        if (LifeTime <= 0)
        {
            gameObject.transform.parent.GetComponentInParent<ShipStats>().shielded = false;
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.transform.parent.GetComponentInParent<ShipStats>().shielded = true;
        }


        LifeTime -= Time.deltaTime;
        
	}

    void OnTriggerEnter(Collider other)
    {
    }
    public void restartShield(float life)
    {
        gameObject.SetActive(true);
        LifeTime = life;
        audioSourceShield.PlayOneShot(audioManager.ShieldActivate, ShieldVolume);
    }
}
