﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public float movespeed;
    public float Damage;
    public GameObject FriendlyParent;
    private Vector3 fwd;
    private RaycastHit rayhit;

    private AudioManager audioManager;
    public AudioSource audioSourceBulletHit;
    public float BulletVolume;
    private float timer = 1.5f;
    // Use this for initialization
    void Start () {

    audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
       
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        movespeed += 1;
        transform.position += transform.forward * movespeed * Time.deltaTime;
        gameObject.GetComponent<CapsuleCollider>().height = movespeed * Time.deltaTime;
        fwd = transform.TransformDirection(transform.forward);

        if (Physics.Raycast(transform.position, fwd, out rayhit, movespeed * Time.deltaTime))
        {
            if (rayhit.transform.gameObject.tag == "Shield")
            {
                Destroy(gameObject);
                return;
            }
            if (rayhit.transform.gameObject.tag == "Player")
            {

                if (!rayhit.transform.gameObject.GetComponentInParent<ShipStats>().shielded || rayhit.transform.gameObject != FriendlyParent)
                {
                    rayhit.transform.gameObject.GetComponentInParent<ShipStats>().HealthDamage(Damage);
                    audioSourceBulletHit.PlayOneShot(audioManager.LaserHit, BulletVolume);

                    if (rayhit.transform.gameObject.GetComponentInParent<ShipStats>().Health <= 0 && rayhit.transform.gameObject.GetComponentInParent<ShipMovement>().isDead != true)
                    {
                        FriendlyParent.GetComponentInParent<ShipStats>().Score += 1;
                    }
                }

                if (rayhit.transform.gameObject != FriendlyParent)
                {
                Destroy(gameObject);
                }
                return;
            }
            if (rayhit.transform.gameObject.tag == "Target")
            {
                FriendlyParent.GetComponentInParent<ShipStats>().AddScore(10);
                rayhit.transform.gameObject.GetComponent<Target>().WasHit();
            }
            if (rayhit.transform.gameObject.tag == "point")
            {
                return;
            }
            if (rayhit.transform.gameObject.tag == "Bullet")
            {
                if (!rayhit.transform.gameObject.GetComponent<Bullet>().FriendlyParent != gameObject.GetComponent<Bullet>().FriendlyParent)
            Destroy(gameObject);
            }
            if (timer <= 0)
            {
                Destroy(gameObject);
            }
        }
    }



    void OnTriggerEnter(Collider other)
    {
      
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponentInParent<ShipStats>() != null)
            {
                if (!other.gameObject.GetComponentInParent<ShipStats>().shielded)
                {
                    if (other.gameObject != FriendlyParent)
                    {
                        audioSourceBulletHit.PlayOneShot(audioManager.LaserHit, BulletVolume);

                        other.gameObject.GetComponentInParent<ShipStats>().HealthDamage(Damage);
                        if (other.gameObject.GetComponentInParent<ShipStats>().Health <= 0 && other.gameObject.GetComponentInParent<ShipMovement>().isDead != true)
                        {
                            FriendlyParent.GetComponentInParent<ShipStats>().Score += 1;

                        }
                    }

                }
                if (other.gameObject != FriendlyParent)
                {

                Destroy(gameObject);
                }
            }
            
        }
        
        //Destroy(gameObject);

    }

}
