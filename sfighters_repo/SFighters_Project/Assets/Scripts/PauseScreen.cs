﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseScreen : MonoBehaviour {

    //Canvas gameObject from Scene
    private GameObject pauseCanvas;

    //PauseManager gameObject from Scene
    private PauseManager pauseManager;

    //Pause Screen toggle tick.
    private Toggle invertTick;

    //Players number index
    private int playerNum;

    //Reference to ShipMovement
    private ShipMovement shipMovement;

	// Use this for initialization
	void Start ()
    {
        //creates variable containing reference to ship movement
        shipMovement = GetComponent<ShipMovement>();

        //creates variable that sets itself to the gameobject tagged with Pause
        pauseCanvas = GameObject.FindGameObjectWithTag("Pause");

        //Creates variable containing reference to pauseManager gameObject
        pauseManager = pauseCanvas.GetComponent<PauseManager>();

        //Sets canvas GameObject to false consistently on startup
        //pauseCanvas.GetComponent<Canvas>().enabled = false;

        //Sets playernum.
        playerNum = int.Parse(gameObject.tag.Substring(1)) - 1;

        //sets pause screen UI
        invertTick = GetComponent<Toggle>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log(InputManager.Devices.Count);
        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (InputManager.Devices[playerNum] == null)
        {
            // If no controller exists for this cube, just make it translucent.
            //Debug.Log("no ctrl");

        }
        else
        {
            //Debug.Log("Controller is okay");
            UpdatePause(InputManager.Devices[playerNum]);
        }
    }

    void UpdatePause(InputDevice inputDevice)
    {
        //If Start is pressed 
        if(inputDevice.Command.WasPressed)
        {
            //Set IsPaused bool to opposite of current state
            pauseManager.IsPaused = !pauseManager.IsPaused;
            if(pauseManager.IsPaused)
            {
                //Set scene time to 0 so it no longer progresses
                Time.timeScale = 0;
                //Enable canvas gameObject
                pauseCanvas.GetComponent<Canvas>().enabled = true;
            }
            else
            {
                //set scene time to 1 so it progresses
                Time.timeScale = 1;
                //Disable canvas gameObject
                pauseCanvas.GetComponent<Canvas>().enabled = false;
            }
            
        }

        if (pauseManager.IsPaused == true)
        {
            //If A is Pressed while on Pause Screen
            if (inputDevice.Action1.WasPressed)
            {
                //if movement is already inverted
                if (shipMovement.invertedY)
                {
                    //Turn off invert
                    shipMovement.invertY = false;
                    shipMovement.invertedY = false;
                    invertTick.isOn = false;
                }
                else
                {
                    //turn on invert
                    shipMovement.invertY = true;
                    shipMovement.invertedY = true;
                    invertTick.isOn = true;
                }
            }
            //If Y is Pressed on Pause Screen, Returns to Main_Menu
            if (inputDevice.RightBumper.IsPressed && inputDevice.LeftBumper.IsPressed && inputDevice.LeftTrigger.IsPressed && inputDevice.RightTrigger.IsPressed)
            {
                Time.timeScale = 1.0f;
                SceneManager.LoadScene("Main_Menu");
            }
        }
    }
}
