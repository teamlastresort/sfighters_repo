﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Interactable : MonoBehaviour {

    public List<InteractableType> objectTypes = new List<InteractableType>();

	public bool IsType(InteractableType a_type)
    {
        return objectTypes.Contains(a_type);
    }
}


public enum InteractableType
{
    DESTROYABLE,
    COLLECTABLE,
    PROJECTILE,
    PLAYER
};