﻿using UnityEngine;
using System.Collections;

public class DestoryDebris : MonoBehaviour {
    public float DeathTimer = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        DeathTimer -= Time.deltaTime;
        if (DeathTimer <= 0)
        {
            Destroy(gameObject);
        }
	}
}
