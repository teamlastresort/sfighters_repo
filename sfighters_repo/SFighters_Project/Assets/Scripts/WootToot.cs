﻿using UnityEngine;
using System.Collections;
using InControl;
public class WootToot : MonoBehaviour {

    public int playerNum;
    public GameObject WootTooter;
    public float WootTootVolume;
    private AudioManager audioManager;
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        WootTooter.GetComponent<AudioSource>().clip = audioManager.WootToot;
        WootTooter.GetComponent<AudioSource>().volume = WootTootVolume;
        playerNum = (int.Parse(gameObject.tag.Substring(1))) - 1;
    }


    void Update()
    {

        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (inputDevice == null)
        {
            // If no controller exists for this cube, just make it translucent.
            
        }
        else
        {
            updateWootToot(inputDevice);
        }
    }
    void updateWootToot(InputDevice inputDevice)
    {
        if (inputDevice.LeftStickButton)
            WootTooter.SetActive(true);
        else
            WootTooter.SetActive(false);
            
        
    }
}
