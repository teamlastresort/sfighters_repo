﻿using UnityEngine;
using System.Collections;

public class BoundsBox : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {

            if(other.GetComponentInParent<ShipStats>() != null)
                other.GetComponentInParent<ShipStats>().InBounds = true;
        }
    }

    void OnTriggerExit(Collider other)
    
    {
        if (other.tag == "Player")
        {
            if (other.GetComponentInParent<ShipStats>() != null)
                other.GetComponentInParent<ShipStats>().InBounds = false;
            
        }

    }
}
