﻿using UnityEngine;
using System.Collections;

public class Floating : MonoBehaviour {
    public float Variation;
    public float Speed;
    private Vector3 MaxUp;
    private Vector3 MaxDown;
    private bool GoingUp;
	// Use this for initialization
	void Start () {
        MaxUp = transform.position + new Vector3(0, Variation, 0);
        MaxDown= transform.position + new Vector3(0, -Variation, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (GoingUp)
        {

            
            transform.position = Vector3.Lerp(transform.position, MaxUp,  Time.deltaTime * Speed);
            if ((transform.position.y - MaxUp.y) > -5 )
                GoingUp = false;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, MaxDown, Time.deltaTime * Speed);
            if ((transform.position.y  - MaxDown.y  ) < 5)
                GoingUp = true;
        }

	}
}
