﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {


    public AudioClip Shooting;
    public AudioClip EngineConstant;
    public AudioClip EngineBoost;
    public AudioClip EMPDrop;
    public AudioClip EMPBlast;
    public AudioClip DeathExplosion;
    public AudioClip LaserHit;
    public AudioClip PointPickup;
    public AudioClip PowerPickup;
    public AudioClip MissileFire;
    public AudioClip MissileTravel;
    public AudioClip ShieldActivate;
    public AudioClip ShieldDeActivate;
    public AudioClip PlayerCollision;
    public AudioClip WootToot;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
