﻿using UnityEngine;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour
{

    public PlayerManager playerManager;
    private List<PlayerScorePair> playerScores = new List<PlayerScorePair>();

    // Use this for initialization
    void Start ()
    {
        //grab the players and create player score pairs
        for(int i = 1; i <= playerManager.PlayerCount; i++)
        {
            PlayerScorePair psp = new PlayerScorePair();
            psp.player = playerManager.GetPlayer(i).GetComponentInChildren<ShipStats>();
            psp.score = psp.player.Score;
            psp.playerID = i;
            psp.trailParent = psp.player.GetComponent<TrailEffect>().trail;
            playerScores.Add(psp);

        }

    }
	
	// Update is called once per frame
	void Update ()
    {

        //update score pairs and save to player prefs
        foreach(PlayerScorePair psp in playerScores)
        {
            psp.score = psp.player.Score;
            PlayerPrefs.SetInt("P" + psp.playerID.ToString() + "Score", psp.player.Score);
        }


        //determine the winner
        playerScores.Sort(delegate (PlayerScorePair p1, PlayerScorePair p2) {
           return (p1.score >= p2.score)? -1:1; } );
        

        //activate the winner's trail (deactivate others)
        for(int i = 0; i < playerScores.Count; i++)
        {
            if (i == 0)
                playerScores[i].trailParent.SetActive(true);
            else
            {
                playerScores[i].trailParent.SetActive(false);
            }

        }

      
 






















        //Player1Score = Player1Stats.Score;
        //Player2Score = Player2Stats.Score;
        //Player3Score = Player3Stats.Score;
        //Player4Score = Player4Stats.Score;


        ////Player1 Score
        //PlayerPrefs.SetInt("P1Score", Player1Stats.Score);

        ////Player2 Score
        //PlayerPrefs.SetInt("P2Score", Player2Stats.Score);

        ////Player3 Score
        //PlayerPrefs.SetInt("P3Score", Player3Stats.Score);

        ////Player4 Score
        //PlayerPrefs.SetInt("P4Score", Player4Stats.Score);


        //if(Player1Score > Player2Score && Player2Score > Player3Score && Player3Score > Player4Score)
        //{
        //    //Player1Stats.gameObject.GetComponentInChildren<TrailRenderer>().material = WinnersTrail;
        //    Player1WinnerAlert.gameObject.SetActive(true);
        //    Player2WinnerAlert.gameObject.SetActive(false);
        //    Player3WinnerAlert.gameObject.SetActive(false);
        //    Player4WinnerAlert.gameObject.SetActive(false);
        //}
        //if (Player2Score > Player1Score && Player1Score > Player3Score && Player3Score > Player4Score)
        //{
        //    //Player2Stats.gameObject.GetComponentInChildren<TrailRenderer>().material = WinnersTrail;
        //    Player2WinnerAlert.gameObject.SetActive(true);
        //    Player1WinnerAlert.gameObject.SetActive(false);
        //    Player3WinnerAlert.gameObject.SetActive(false);
        //    Player4WinnerAlert.gameObject.SetActive(false);
        //}
        //if(Player3Score > Player1Score && Player1Score > Player2Score && Player2Score > Player4Score)
        //{
        //    //Player3Stats.gameObject.GetComponentInChildren<TrailRenderer>().material = WinnersTrail;
        //    Player3WinnerAlert.gameObject.SetActive(true);
        //    Player1WinnerAlert.gameObject.SetActive(false);
        //    Player2WinnerAlert.gameObject.SetActive(false);
        //    Player4WinnerAlert.gameObject.SetActive(false);
        //}
        //if (Player4Score > Player1Score && Player4Score > Player2Score && Player4Score > Player3Score)
        //{
        //    // Player4Stats.gameObject.GetComponentInChildren<TrailRenderer>().material = WinnersTrail;
        //    Player4WinnerAlert.gameObject.SetActive(true);
        //    Player1WinnerAlert.gameObject.SetActive(false);
        //    Player2WinnerAlert.gameObject.SetActive(false);
        //    Player3WinnerAlert.gameObject.SetActive(false);
        //}

        //if (Player1Score == Player2Score && Player2Score == Player3Score && Player3Score == Player4Score)
        //{
        //    Player1WinnerAlert.gameObject.SetActive(false);
        //    Player2WinnerAlert.gameObject.SetActive(false);
        //    Player3WinnerAlert.gameObject.SetActive(false);
        //    Player4WinnerAlert.gameObject.SetActive(false);
        //}



    }
}

[System.Serializable]
public class PlayerScorePair
{
    public int playerID;
    public ShipStats player;
    public int score;
    public GameObject trailParent;
}