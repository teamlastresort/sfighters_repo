﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using InControl;
public class SelectionInput : MonoBehaviour
{

    
    //Ship Models
    public GameObject[] shipModels = new GameObject[4];

    //Controllers
    private InputDevice[] inputDevice = new InputDevice[4];

    //ShipID
    private int[] shipIndex = {0,0,0,0 };

    //Arrow Array - Scene GameObject
    public Image[] Arrow = new Image[4];

    //Ship Array - Scene GameObject
    public GameObject[] Ship = new GameObject[4];

    //Text above ships - Scene GameObject
    public GameObject[] shipText = new GameObject[4];


    private int[] PlayerChoice = {1,2,3,4 };

    private bool[] hasSelected = { false, false, false, false };


    public GameObject StartText;
    public static int playerNum = 0;
    private int TotalSelected;

    private string PlayerName;
    // Use this for initialization
    void Start ()
    {
        Debug.Log(PlayerPrefs.GetInt("PlayerNum"));
        for (int i = 0; i < 4; i++)
        {

            shipText[i].gameObject.SetActive(false);
            
            Ship[i].SetActive(false);

            Arrow[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < 4; i++)
        {

            inputDevice[i] = (InputManager.Devices.Count > i) ? InputManager.Devices[i] : null;
        }
    }
    void Update()
    {
        playerNum = InputManager.Devices.Count;
        
        for (int i = 0; i < playerNum ; i++)
        {
            if (inputDevice[i] != null)
            {
            inputDevice[i].Vibrate(1.0f, 2.0f);
            UpdateSelection(inputDevice[i], i);

            }
        }

    }

    // Update is called once per frame
    void UpdateSelection (InputDevice inputDevice, int PNum)
    {
        PlayerName = "P" + (PNum + 1) + "Choice";
        
        if (inputDevice.Action1.WasPressed)
        {
            shipText[PNum].gameObject.SetActive(true);

            
            Ship[PNum].SetActive(true);
            Arrow[PNum].gameObject.SetActive(true);
        }

        if (hasSelected[PNum] == false && inputDevice.DPadRight.WasPressed)
        {
            shipIndex[PNum]++;
        
            if (shipIndex[PNum] >= shipModels.Length)
                shipIndex[PNum] = 0;
            
            Ship[PNum] = SwapShipMesh(Ship[PNum], shipModels[shipIndex[PNum]]);
        
        
           Ship[PNum].transform.localScale = Vector3.one * 1.25f;
                   
        }
            ////create new


          
        
        if (inputDevice.Action3)
        {
            Arrow[PNum].gameObject.SetActive(false);
            PlayerChoice[PNum] = shipIndex[PNum];

            PlayerPrefs.SetInt(PlayerName ,PlayerChoice[PNum]);
            hasSelected[PNum] = true;
            



        }
        TotalSelected = 0;
        for (int i = 0; i < hasSelected.Length; i++)
        {
            if (hasSelected[i])
            {
                TotalSelected++;
            }
            
        }

        if(TotalSelected >= 2 && inputDevice.CommandWasPressed)
        {
            PlayerPrefs.SetInt("PlayerNum", PNum);

            GetComponent<SwitchScene>().LoadMapSelect();
        }
        if (TotalSelected >= 2)
        {
            StartText.SetActive(true);
        }
        else
        {
            StartText.SetActive(false);
        }

    }
    

    private GameObject SwapShipMesh(GameObject originalShip, GameObject newShip)
    {
        Vector3 pos = originalShip.transform.position;
        Quaternion rot = originalShip.transform.rotation;
        Destroy(originalShip);
        return (GameObject)Instantiate(newShip, pos, rot);
    }
}
