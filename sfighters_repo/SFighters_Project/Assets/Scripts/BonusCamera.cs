﻿using UnityEngine;
using System.Collections;

public class BonusCamera : MonoBehaviour {
    public Rect CamRect;

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetInt("PlayerNum") == 2)
        {
            CamRect.x = 0.5f;
            CamRect.y = 0f;
            CamRect.width = .5f;
            CamRect.height = .5f;
        }
        else
        {
            gameObject.SetActive(false);
        }
        gameObject.GetComponent<Camera>().rect = CamRect;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
