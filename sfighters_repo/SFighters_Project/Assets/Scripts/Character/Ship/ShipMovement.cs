﻿using UnityEngine;
using System.Collections;
using InControl;
public class ShipMovement : MonoBehaviour {

    private float x;
    private float y;
    private float BoostTimer;
    private float LockTimer;
    private float BarrelTimer;
    public float BoostConsumption;
    public float BoostDownTime;
    public float BoostFuelMax;
    public float BoostFuel;
    public float BoostRegen;
    public float BarrelRollSpeed;
    public float BoostSpeed;
    public float ChildRot;
    public float curSpeed;
    public float FlipSpeed;
    public float MaxThrottle;
    public float Throttle;
    public float LockDuration;
    public float movespeed;
    public float RotSpeed;
    public float ShipMaxRoll;
    public bool Boosted = false;
    public bool CanControl = true;
    public bool invertY = false;
    public bool invertX = false;
    private bool rolling = false;
    private bool RollRight = false;
    private bool BarrelSet = false;
    private bool Rotating = false;
    private bool BoostTimerCheck = false;
    private bool TurnLock = false;
    private bool boostLockOut;
    private Quaternion LockOutRotation;
    public GameObject ChildMesh;
    public GameObject ShipTarget;
    private Vector3 Direction;
    private Vector3 ChildDirection;
    private ControllerConfig controllerconfig;

    private Vector2 AnalogAngle;
    private float Angle;
    private bool Barreling = false;


    public float MaxInput;
    public float MaxMoveDist;
    public float ReturnSpeed;
    private float CurX;
    private float CurY;
    private Vector3 CurMovement;
    public bool analogSetMove;

    private AudioManager audioManager;
    public AudioSource audioSourceEngine;
    public float EngineVolume;

    private bool SideReleased;
    private bool RightBumper;
    private bool LeftSet = false;

    public  bool invertedY;
    public  bool invertedX;

    private float FlipTimer;
    private bool Flipping = false;

    private Rigidbody rigidBody;

    public bool isDead = false;


    public GameObject DeathParticles;

    private Quaternion TempStore;

    private bool FlipStarted = false;

    private int playerNum;



    private float RotationStore = 0;
    private bool OverShootHit = false;
    public float OverFlow = 5;
    public float OverFlowReSetTime;
    public float OverFlowRotation;
    public float OverFlowXChange;

    private float OverFlowCap;

    private bool OverFlip = false;
    private bool OverFlipSet = false;
    private float OverFlipTimer;
    public float OverFlipmaxTime;
    public GameObject[] Debris;
    public GameObject ExplosionParticle;
    private float DeathTimer = 2;
    private bool DeathTimerSet = false;

    private Quaternion BackRotation;
    private float CurXStore;
    private float CurYStore;

    private Transform TempChild;
    // Use this for initialization
    void Start () {
        BarrelTimer = 0.1f;
        OverFlowCap = OverFlow;

        playerNum = int.Parse(gameObject.tag.Substring(1)) - 1;

        FlipTimer = 1;
        invertedX = invertX;
        invertedY = invertY;

        ChildDirection = ShipTarget.transform.position - ChildMesh.transform.position;
        ChildDirection.Normalize();
        Quaternion targetRotation = Quaternion.LookRotation(ChildDirection);
        ChildMesh.transform.rotation = targetRotation;

        controllerconfig = GetComponent<ControllerConfig>();
        LockTimer = LockDuration;
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        audioSourceEngine.volume = EngineVolume;
        audioSourceEngine.clip = audioManager.EngineConstant;
        audioSourceEngine.Play();

        rigidBody = gameObject.GetComponent<Rigidbody>();

        TempStore = transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(InputManager.Devices.Count);
        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (InputManager.Devices[playerNum] == null)
        {
            // If no controller exists for this cube, just make it translucent.
            Debug.Log("no ctrl");
            
        }
        else
        {
          //  Debug.Log("Controller is okay");
            UpdateMovement(InputManager.Devices[playerNum]);
        }
    }
    void UpdateMovement(InputDevice inputDevice)
    {
           curSpeed = 100;

        if (!isDead)
        {

            DeathParticles.SetActive(false);
            DeathTimer = 2;
            DeathTimerSet = false;
            ChildMesh.SetActive(true);
            

            if (CanControl)
            {
                if (inputDevice.RightTrigger >= .2)
                {
                    gameObject.GetComponent<Shoot>().Shoots();
                }
                else
                {
                    gameObject.GetComponent<Shoot>().NotShoot();

                }
                rigidBody.useGravity = false;
                    rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
                if (!Flipping)
                {


                    if (inputDevice.LeftBumper && SideReleased || inputDevice.RightBumper && SideReleased)
                    {
                        RightBumper = inputDevice.RightBumper ? true : false;
                        
                        
                        SideReleased = false;
                        Flipping = true;
                        OverFlipTimer = OverFlipmaxTime;
                    }
                    else 
                    {
                        SideReleased = true;
                    }

                    //if barrel roll is happening call func
                    if (rolling)
                    {
                        BarrelRoll();
                    }
                    else if (inputDevice.Action3)
                    {
                        if (inputDevice.LeftStick.X > 0.8f)
                        {
                            rolling = true;
                            TurnLock = true;
                        }
                        if (inputDevice.LeftStick.X < -.8f)
                        {
                            rolling = true;
                            TurnLock = true;
                        }
                       
                    }
                }
                else
                {
                    if (!RightBumper)
                        transform.rotation = transform.rotation * Quaternion.AngleAxis(-FlipSpeed * Time.deltaTime, new Vector3(0, 0, 1));
                    else
                        transform.rotation = transform.rotation * Quaternion.AngleAxis(FlipSpeed * Time.deltaTime, new Vector3(0, 0, 1));


                    if (RotationStore >= 180 - FlipSpeed * Time.deltaTime)
                    {
                    TempStore = transform.rotation;
                        if (transform.rotation.eulerAngles.z > 175 - FlipSpeed * Time.deltaTime && transform.rotation.eulerAngles.z < 185 + FlipSpeed * Time.deltaTime)
                        {
                            TempStore.eulerAngles = new Vector3(TempStore.eulerAngles.x, TempStore.eulerAngles.y, 180);
                            transform.rotation = TempStore;
                            
                        }
                        else
                        {
                            TempStore.eulerAngles = new Vector3(TempStore.eulerAngles.x, TempStore.eulerAngles.y, 0);
                            transform.rotation = TempStore;
                        }


                        LeftSet = false;
                        Flipping = false;
                        FlipStarted = false;
                        RotationStore = 0;
                    }


                    RotationStore += FlipSpeed * Time.deltaTime;
                }

            }
            else
            {
                //LockOutRotation = Quaternion.Euler(new Vector3(90.0f, 0, 0));
                //transform.rotation = Quaternion.Lerp(transform.rotation, LockOutRotation, 0.1f * Time.deltaTime);
                rigidBody.useGravity = true;
                rigidBody.constraints = RigidbodyConstraints.None;
                rigidBody.constraints = RigidbodyConstraints.FreezeRotationZ;
                rigidBody.constraints = RigidbodyConstraints.FreezeRotationX;

            }

        }
        else
            Death();
        
            Move(inputDevice);
    
        
    }


    void BarrelRoll()
    {
        if (!BarrelSet)
        {
            BarrelSet = true;
            AnalogAngle = new Vector2(x, y);
            Angle = Vector2.Angle(Vector2.up, AnalogAngle);
            BarrelTimer = 0.3f;
            CurXStore = CurX;
            CurYStore = CurY;
            TempChild = ChildMesh.transform;

        }

        if (!Rotating)
        {
            if (Angle <= 135 && Angle > 45)
            {
                if (x < -0.1)
                {
                Direction = -transform.right;
                RollRight = false;

                }
                else
                {
                    Direction = transform.right;
                    RollRight = true;
                }
                Rotating = true;
                TurnLock = true;
                Barreling = true;
            }

        }
        if (Barreling)
        {

            if (OverFlip)
            {
                if (RollRight)
                {
                    rigidBody.AddForce(Direction * curSpeed);
                    ChildMesh.transform.Rotate(new Vector3(0, 0, -BarrelRollSpeed * Time.deltaTime * OverFlipTimer));
                }
                if (!RollRight)
                {
                    rigidBody.AddForce(Direction * curSpeed);
                    ChildMesh.transform.Rotate(new Vector3(0, 0, BarrelRollSpeed * Time.deltaTime * OverFlipTimer));
                }
            }
            else
            {

                if (RollRight)
                {
                    rigidBody.AddForce( Direction * curSpeed);
                    ChildMesh.transform.Rotate(new Vector3(0, 0, -BarrelRollSpeed * Time.deltaTime));
                }
                if (!RollRight)
                {
                    rigidBody.AddForce( Direction * curSpeed);
                    ChildMesh.transform.Rotate(new Vector3(0, 0, BarrelRollSpeed * Time.deltaTime));
                }
            }

            //TempChild.Rotate(new Vector3(0, 0, -CurX * (ShipMaxRoll + OverFlow * OverFlowRotation)));

             
            if (Vector3.Angle(ChildMesh.transform.up, transform.up) <= BarrelRollSpeed * Time.deltaTime && BarrelTimer <= 0)
            {
                if (!OverFlipSet)
                {
                    OverFlip = true;
                    OverFlipSet = true;
                    if (Direction == transform.right)
                    {
                        ChildMesh.transform.Rotate(new Vector3(0, 0, -BarrelRollSpeed * Time.deltaTime / 4));
                    }
                    else
                    {
                        ChildMesh.transform.Rotate(new Vector3(0, 0, BarrelRollSpeed * Time.deltaTime / 4));
                    }
                }
                
                else if(OverFlipTimer <=0)
                {
                    CurX = CurXStore;
                    CurY = CurYStore;
                    rolling = false;
                    Rotating = false;
                    TurnLock = false;
                    BarrelSet = false;
                    Barreling = false;
                    OverFlipSet = false;
                    OverFlip = false;
                    OverFlipTimer = OverFlipmaxTime;
                }
            }
            if (OverFlip)
            {
                OverFlipTimer -= Time.deltaTime;
            }
            BarrelTimer -= Time.deltaTime;
        }
    }
    
    public void LockControls()
    {
        CanControl = false; 
    }
    public float CurrentMoveSpeed()
    {

        return (movespeed * curSpeed);

    }
    private void Move(InputDevice inputDevice)
    {
        if (!boostLockOut)
        {

            if (inputDevice.Action2)
            {
                if (!Boosted)
                {
                    if (audioSourceEngine.clip != audioManager.EngineBoost)
                    {
                        audioSourceEngine.clip = audioManager.EngineBoost;
                        audioSourceEngine.Play();
                    }
                    movespeed += BoostSpeed;
                }
                //adds boost once and turns bool on
                Boosted = true;
                //boost fuel is reduced

            }
            else
            {
                if (Boosted)
                {
                    if (audioSourceEngine.clip != audioManager.EngineConstant)
                    {
                        audioSourceEngine.clip = audioManager.EngineConstant;
                        audioSourceEngine.Play();
                    }
                    movespeed -= BoostSpeed;
                }
                    //takes boost off once and turns bool off
                Boosted = false;
                //boost regenerates
            }

            if (Boosted == true)
            {
                if (BoostFuel >= 0)
                    BoostFuel -= BoostConsumption * Time.deltaTime;
                else
                {
                    Boosted = false;
                    movespeed -= BoostSpeed;
                    BoostFuel = 0;
                    boostLockOut = true;
                }

            }
            else
            {
                if (BoostFuel <= BoostFuelMax)
                    BoostFuel += BoostRegen * Time.deltaTime;
                else
                    BoostFuel = BoostFuelMax;
                

            }
        }
        else
        {
            if (!BoostTimerCheck)
            {
                BoostTimer = BoostDownTime;
                BoostTimerCheck = true;
            }
            BoostTimer -= Time.deltaTime;
            BoostFuel += BoostRegen * Time.deltaTime;
            if (BoostTimer < 0)
            {
                BoostTimerCheck = false;
                boostLockOut = false;
            }
            if (BoostFuel > BoostFuelMax)
            {
                BoostFuel = BoostFuelMax;
            }
        }
        if (CanControl && !isDead)
        {
            if(!invertX)
                x = inputDevice.LeftStick.X;
            else
                x = -inputDevice.LeftStick.X;
            if (!invertY)
                y = inputDevice.LeftStick.Y;
            else
                y = -inputDevice.LeftStick.Y;

            if (analogSetMove)
            {

                if (x > 0.16)
                {
                    CurX += Mathf.Abs(x) * MaxInput * Time.deltaTime;
                    if (CurX > x)
                    {
                        if (Mathf.Abs(CurX)- Mathf.Abs(x) < MaxInput * Time.deltaTime)
                        {
                            CurX = x;
                        }
                        else
                        {
                            CurX -= x * MaxInput * Time.deltaTime * 2;
                        }
                    }
                }
                else if (x < -0.16)
                {
                    CurX -= Mathf.Abs(x) * MaxInput * Time.deltaTime;
                    if (CurX < x)
                    {
                        if (Mathf.Abs(CurX) - Mathf.Abs(x) < MaxInput * Time.deltaTime)
                        {
                            CurX = x;
                        }
                        else
                        {
                            CurX += x * MaxInput * Time.deltaTime * 2;
                        }
                    }
                }
                else
                    CurX = Mathf.Lerp(CurX, 0, MaxInput * Time.deltaTime);


                if (y > 0.16)
                {
                    CurY += Mathf.Abs(y) * MaxInput * Time.deltaTime;
                    if (CurY > y)
                    {
                        if (Mathf.Abs(CurY) - Mathf.Abs(y) < MaxInput * Time.deltaTime)
                        {
                            CurY = y;
                        }
                        else
                        {
                            CurY -= y * MaxInput * Time.deltaTime * 2;
                        }
                    }
                }
                else if (y < -0.16)
                {
                    CurY -= Mathf.Abs(y) * MaxInput * Time.deltaTime;
                    if (CurY < y)
                    {
                        if (Mathf.Abs(CurY) - Mathf.Abs(y) < MaxInput * Time.deltaTime)
                        {
                            CurY = y;
                        }
                        else
                        {
                            CurY += y * MaxInput * Time.deltaTime * 2;
                        }
                    }
                }
                else
                        CurY = Mathf.Lerp(CurY, 0, MaxInput * Time.deltaTime);
                
                
            }
            else
            {

                CurX += x * MaxInput * Time.deltaTime;

                CurY += y * MaxInput * Time.deltaTime;
           
                
            }

            if (Mathf.Abs(CurY) > MaxMoveDist)
                if (CurY > 0)
                    CurY = MaxMoveDist;
                else
                    CurY = -MaxMoveDist;
            if (Mathf.Abs(CurX) > MaxMoveDist)
                if (CurX > 0)
                    CurX = MaxMoveDist;
                else
                    CurX = -MaxMoveDist;
            if (y < 0)
                CurMovement.y = y * 50;
            else
                CurMovement.y = -y * 12;
            //animate the ship
                ShipTarget.transform.localPosition = new Vector3(CurX * 30, CurY * 20, 100);
                
                //ShipTarget.transform.localPosition = new Vector3(0, y * 20, 100);
            
            ChildMesh.transform.localPosition = new Vector3(-CurX * (3 + (OverFlow * OverFlowXChange)), -CurY * 3);
        }
        else
        {
            LockTimer -= Time.deltaTime;
            if (LockTimer <= 0)
            {
                CanControl = true;
                LockTimer = LockDuration;
            }
        }



        if (!TurnLock)
        {



                transform.rotation = Quaternion.Euler(new Vector3(0.0f, CurX * RotSpeed * Time.deltaTime, 0.0f)) * transform.rotation;

                transform.rotation = Quaternion.Euler(transform.right * -CurY * RotSpeed * Time.deltaTime) * transform.rotation;



                ChildDirection = ShipTarget.transform.position - ChildMesh.transform.position;
                ChildDirection.Normalize();
                Quaternion targetRotation = Quaternion.LookRotation(ChildDirection,transform.up);
                ChildMesh.transform.rotation = targetRotation;
            




            if (Vector3.Angle(Vector3.up, transform.up) >= 90 && Vector3.Angle(Vector3.up, transform.up) <=180)
            {
                if (invertedY)
                    invertY = true;
                else
                    invertY = false;

                if (invertedX)
                    invertX = true;
                else      
                    invertX = false;



            }
            else
            {
                if (invertedY)
                    invertY = true;
                else
                    invertY = false;
                if (invertedX)
                    invertX = true;
                else
                    invertX = false;
            }


            //TurnStore = Mathf.Lerp(ChildMesh.transform.rotation.eulerAngles.z, MaxMoveDist, MaxInput * Time.deltaTime);

            if (!OverShootHit)
            {
                ChildMesh.transform.Rotate(new Vector3(0, 0, -CurX * (ShipMaxRoll + OverFlow * OverFlowRotation )));

                if (Vector3.Angle(transform.right, ChildMesh.transform.right) > ShipMaxRoll + (OverFlow * (OverFlowRotation -.25f)))
                {
                    OverShootHit = true;
                }
            }
            else
            {
                
                
                    if (OverFlow > 0)
                    {
                        OverFlow -=   OverFlowReSetTime * OverFlowCap * Time.deltaTime;
                    }
                    if (OverFlow < 0)
                    {
                        OverFlow = 0;
                    }



                ChildMesh.transform.Rotate(new Vector3(0, 0, -CurX * (ShipMaxRoll + OverFlow * OverFlowRotation)));
                if (Vector3.Angle(transform.right, ChildMesh.transform.right) < ShipMaxRoll/OverFlowCap)
                {
                    OverFlow = OverFlowCap;
                    OverShootHit = false;
                }



            }
            //Debug.Log(Vector3.Angle(transform.right, ChildMesh.transform.right));



        //            Debug.Log(ChildMesh.transform.rotation.eulerAngles.z);




    }

        Vector3 currentMovement = Vector3.zero;
        currentMovement += transform.forward * movespeed * Time.deltaTime;

        
        Throttle = inputDevice.LeftTrigger * 100;

        if (Throttle > MaxThrottle)
            Throttle = MaxThrottle;

        curSpeed -= Throttle;


        


        currentMovement *= curSpeed;
        if (CurMovement.magnitude > movespeed)
        {
            CurMovement = CurMovement.normalized * movespeed;
        }

        rigidBody.velocity = Vector3.zero;

            rigidBody.AddForce( currentMovement);

        

        if (rigidBody.velocity.magnitude > movespeed)
        {
            currentMovement = CurMovement.normalized * movespeed;
        }
        //cap rigidbody.velocity

        
    }


    public float ReturnX()
    {
        return x;
    }
    public float ReturnY()
    {
        return y;
    }
    public void Reset()
    {
        TurnLock = false;
        Rotating = false;
        rolling = false;
        BarrelSet = false;
        LockTimer = LockDuration;
        Barreling = false;
        CanControl = true;
        boostLockOut = false;
        BoostTimerCheck = false;
        BoostTimer = BoostDownTime;
        BoostFuel = BoostFuelMax;
        isDead = false;

        Flipping = false;
        FlipStarted = false;
        RotationStore = 0;
        OverFlipTimer = OverFlipmaxTime;

    }
    private void Death()
    {

        if (DeathTimer <= 0 && !DeathTimerSet)
        {
            for (int i = 0; i < Debris.Length; i++)
            {
                Instantiate(Debris[i], transform.position, Random.rotation);
            }
            Instantiate(ExplosionParticle, transform.position, transform.rotation);
            DeathTimerSet = true;
            ChildMesh.SetActive(false);
        }
        if (DeathTimerSet)
        {
            curSpeed = 0;
        }
        DeathParticles.SetActive(true);
        LockOutRotation = Quaternion.Euler(new Vector3(90.0f, 0, 0));
        transform.rotation = Quaternion.Lerp(transform.rotation, LockOutRotation, 0.1f * Time.deltaTime);

        DeathTimer -= Time.deltaTime;
    }
}
