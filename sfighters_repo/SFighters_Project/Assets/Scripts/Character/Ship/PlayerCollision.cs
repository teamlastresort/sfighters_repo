﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {
    
    //FX particles
    public GameObject hitParticles;
    public GameObject Ship;
    public float NoseDamage;
    public float BodyDamage;
    public float TBoneDamage;
    public float noseCheckSphere;
    public float CollisionVolume;


    public AudioSource audioSourceShip;
    public AudioClip audioCollision;

    public Transform NosePoint;
    public GameObject Tree;
    public int maxTreeRange;
    private AudioManager audioManager;
    private bool ShieldOn = false;

	// Use this for initialization
	void Start () {
        //audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        //audioSourceShip = transform.parent.parent.GetComponent<AudioSource>();
        //audioSourceShip.volume = CollisionVolume;
        //audioSourceShip.clip = audioManager.PlayerCollision;
        BodyDamage *= Time.deltaTime;
    }

    void OnCollisionEnter(Collision other)
    {
        Collider[] hitColliders = Physics.OverlapSphere(NosePoint.position, noseCheckSphere);
        
        for (int i = 0; i <hitColliders.Length; i ++)
        {
            if (hitColliders[i].gameObject.tag == "Collidables")
            {
                Ship.GetComponent<ShipStats>().HealthDamage(NoseDamage);
                if (Ship.GetComponent<ShipStats>().Health <= 0)
                {
                    ContactPoint contact = other.contacts[0];
                    Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
                    Vector3 pos = contact.point;

                    Tree.transform.position = pos;
                    Tree.transform.rotation = rot;
                    if (Random.Range(0, maxTreeRange) == 0)
                        Instantiate(Tree);

                    gameObject.GetComponent<ShipStats>().DiedToWall = true;
                }
                return;
            }
            if (hitColliders[i].gameObject.tag == "Player" && hitColliders[i].gameObject != Ship.GetComponent<ShipMovement>().ChildMesh)
            {
                if (!(Ship.GetComponent<ShipStats>().shielded))
                {
                    Ship.GetComponent<ShipStats>().HealthDamage(TBoneDamage);
                }

                hitColliders[i].gameObject.GetComponentInParent<ShipStats>().HealthDamage(NoseDamage);
            }

        }
        Ship.GetComponent<ShipStats>().HealthDamage(BodyDamage);

        
        
       
    }
    void OnCollisionStay(Collision other)
    {
        Collider[] hitColliders = Physics.OverlapSphere(NosePoint.position, noseCheckSphere);

        for (int i = 0; i < hitColliders.Length; i++)
        {

            if (hitColliders[i].gameObject.tag == "Collidables")
            {
                ContactPoint contact = other.contacts[0];
                Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
                Vector3 pos = contact.point;

                Tree.transform.position = pos;
                Tree.transform.rotation = rot;
                if (Random.Range(0, maxTreeRange) == 0)
                    Instantiate(Tree);

                Ship.GetComponent<ShipStats>().HealthDamage(NoseDamage);
                if (Ship.GetComponent<ShipStats>().Health <= 0)
                {
                    gameObject.GetComponent<ShipStats>().DiedToWall = true;
                }
                return;
            }
            if (hitColliders[i].gameObject.tag == "Player" && gameObject.GetComponent<ShipMovement>().isDead == false)
            {
                if (!(Ship.GetComponent<ShipStats>().shielded))
                {
                    Ship.GetComponent<ShipStats>().HealthDamage(TBoneDamage);
                    if (hitColliders[i].gameObject.GetComponent<ShipStats>() != null)
                    {

                    hitColliders[i].gameObject.GetComponent<ShipStats>().HealthDamage(100);
                    }
                }

            }

        }
        Ship.GetComponent<ShipStats>().HealthDamage(BodyDamage);



    }


    public void OnDrawGizmos()
    {

        Gizmos.DrawSphere(NosePoint.position, noseCheckSphere);
        


    }


}
