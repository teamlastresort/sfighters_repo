﻿using UnityEngine;
using System.Collections;
using InControl;
public class Shoot : MonoBehaviour {
    public GameObject Ship;
    public GameObject Bullet;
    private ControllerConfig controllerConfig;
    public float MaxHeat;
    public float HeatGen;
    public float CoolDown;
    public float CurHeat;
    public float LockOutDuration;
    private float lockout;
    public bool OverHeated;
    private bool TimerCheck;
    private AudioManager audioManager;
    public AudioSource audioSourceShoot;
    public float Volume;

    private int playerNum;

    public float TimerLength;
    private float timer;
    // Use this for initialization
    void Start ()
    {
        playerNum = (int.Parse(gameObject.tag.Substring(1))) - 1;

        TimerCheck = false;
        OverHeated = false;
        controllerConfig = GetComponent<ControllerConfig>();
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        audioSourceShoot.volume = Volume;
        timer = TimerLength;
    }
    void Update()
    {
        
    }
    // Update is called once per frame
    public void Shoots () {

        if (timer >= 0)
        {
        timer -= Time.deltaTime;
        }
        if (!OverHeated)
        {
            Bullet.transform.position = gameObject.GetComponentInParent<ShipMovement>().ChildMesh.transform.position + (gameObject.transform.forward * 7);
            Bullet.transform.rotation = gameObject.GetComponentInParent<ShipMovement>().ChildMesh.transform.rotation;
            Bullet.GetComponent<Bullet>().FriendlyParent = Ship;
            
        
                if (timer <= 0)
                {

                    if (CurHeat < MaxHeat)
                    {
                        Instantiate(Bullet);
                        if (audioSourceShoot.clip != audioManager.Shooting)
                            audioSourceShoot.clip = audioManager.Shooting;
                        if(!audioSourceShoot.isPlaying);
                               audioSourceShoot.Play();
                        CurHeat += HeatGen;
                    }
                    
                    if (CurHeat >= MaxHeat)
                    {
                        CurHeat = MaxHeat;
                        OverHeated = true;
                    }
                    timer = TimerLength;
                }

            
            
        }
        else
        {
            if (!TimerCheck)
            {
                lockout = LockOutDuration;
                TimerCheck = true;
            }
            if (lockout > 0)
            {
                lockout -= Time.deltaTime;
                CurHeat -= CoolDown * Time.deltaTime;
                if (CurHeat <= 0)
                {
                    CurHeat = 0;
                }
            }
            else
            {
                OverHeated = false;
                TimerCheck = false; 
            }
        }
        
	}
    public void NotShoot()
    {
        CurHeat -= CoolDown * Time.deltaTime;
        if (CurHeat <= 0)
        {
            CurHeat = 0;
        }

        if (OverHeated)
            
        {
            if (!TimerCheck)
            {
                lockout = LockOutDuration;
                TimerCheck = true;
            }
            if (lockout > 0)
            {
                lockout -= Time.deltaTime;
                CurHeat -= CoolDown * Time.deltaTime;
                if (CurHeat <= 0)
                {
                    CurHeat = 0;
                }
            }
            else
            {
                OverHeated = false;
                TimerCheck = false;
            }
        }
        
    }

}
