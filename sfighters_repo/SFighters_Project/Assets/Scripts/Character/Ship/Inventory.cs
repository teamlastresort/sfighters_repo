﻿using UnityEngine;
using System.Collections;
using InControl;
public class Inventory : MonoBehaviour {
    private bool HasMissile = false;
    private bool HasShield = false;
    private bool HasEMP = false;
    public float ShieldLife;
    public GameObject Missile;
    public GameObject Shield;
    public GameObject EMP;


    private AudioManager audioManager;
    public AudioSource audioSourceItem;
    public float ItemVolume;

    private ControllerConfig controllerconfig;
    private int playerNum;
    // Use this for initialization
    void Start () {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        controllerconfig = GetComponent<ControllerConfig>();
        playerNum = int.Parse(gameObject.tag.Substring(1)) - 1;


    }

    void Update()
    {
        var inputDevice = (InputManager.Devices.Count > playerNum) ? InputManager.Devices[playerNum] : null;
        if (InputManager.Devices[playerNum] == null)
        {
            // If no controller exists for this cube, just make it translucent.
//            Debug.Log("no ctrl");

        }
        else
        {
           // Debug.Log("Controller is okay");
            UpdateInv(InputManager.Devices[playerNum]);
        }
    }
    // Update is called once per frame
    void UpdateInv (InputDevice inputDevice) {
        if (inputDevice.Action1)
        {
            UseItem();
        }
        if (Input.GetKey(KeyCode.Keypad1))
            AddItem(0);
        if (Input.GetKeyDown(KeyCode.Keypad2))
            AddItem(1);
        if (Input.GetKeyDown(KeyCode.Keypad3))
            AddItem(2);
    }

    void UseItem()
    {      
         if(HasMissile)
         {
            Missile.transform.position = transform.position + transform.forward + (-transform.up * 4);
            Missile.transform.rotation = transform.rotation;
            Missile.GetComponent<MissileMovement>().Target = GameObject.FindGameObjectWithTag("P1");
            Missile.GetComponent<MissileMovement>().Friendly = gameObject;
            Instantiate(Missile);
            HasMissile = false;
            audioSourceItem.PlayOneShot(audioManager.MissileFire, ItemVolume);
        }
        if(HasShield)
        {
            Shield.GetComponent<Shield>().restartShield(ShieldLife);
            gameObject.GetComponent<ShipStats>().shielded = true;
            HasShield = false;
        }
        if(HasEMP)
        {
            EMP.transform.position = transform.position + -transform.forward + (-transform.up * 2);
            Instantiate(EMP);
            HasEMP = false;
            audioSourceItem.PlayOneShot(audioManager.EMPDrop, ItemVolume);
        }
    }
    public void AddItem(int a_num)
    {
        a_num %= 3;
        HasMissile = false;
        HasShield = false;
        HasEMP = false;

        switch (a_num)
        {
            case 0:
                HasMissile = true;
                break;
            case 1:
                HasShield = true;
                break;
            case 2:
                HasEMP = true;
                break;
            default:
                break;
        }
    }
    public string CurrentPower()
    {
        if (HasMissile)
            return "Missile";
        if (HasEMP)
            return "EMP";
        if (HasShield) 
            return "Shield";
           
        return "No power-up";
    }
}
