﻿using UnityEngine;
using System.Collections;

public class BoostFX : MonoBehaviour {

    private bool Boosted;
    private ShipMovement shipMovement;
    public ParticleSystem boostParticles;

	// Use this for initialization
	void Start ()
    {
        shipMovement = GetComponent<ShipMovement>();

    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(shipMovement.Boosted == true)
        {
            if(boostParticles.isPlaying == false)
            {
                boostParticles.Play();
            }
        }
        else
        {
            boostParticles.Stop();
        }
	}
}
