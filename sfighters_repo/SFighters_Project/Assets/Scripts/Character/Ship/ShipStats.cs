﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class ShipStats : MonoBehaviour {
    public float MaxHealth;
    public float Health;
    public float BoundsDeathTimer;
    public float BoundsDeathTimerMax;
    public float ExplosionVolume;
    public int Score;
    public bool shielded = false;
    public bool InBounds = true;
    public bool DiedToWall = false;

    public GameObject PointDrop;
    public AudioSource audioSourceDeath;
    private AudioManager audioManager;


    public Image BoundsPanel;
    public Image DamagePanel;
    public Image EMPPanel;
    public float boundsFlashTimer;
    public float MaxAlpha;
    public float alphachange = 0.05f;
    private float curAlpha;
    private Color colour;
    
    private GameObject[] RespawnList;
    private bool RespawnSet = false;

    public float MaxDamageTimer;
    private bool damageTaken = false;
    private float damageTimer;
    private float damageCurAlpha = 0;
    private Color damagecolour;

    public float MaxEMPTimer;
    private float EMPTimer;
    private float EMPCurAlpha = 0;
    private Color EMPcolour;

    
    public GameObject deathFX;
    public ParticleSystem explosionFX;
    
    // Use this for initialization
    void Start () {
        RespawnList = GameObject.FindGameObjectsWithTag("Respawn");
        BoundsDeathTimerMax = 7;
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        audioSourceDeath.clip = audioManager.DeathExplosion;
        audioSourceDeath.volume = ExplosionVolume;
        Health = MaxHealth;
        damageTimer = MaxDamageTimer;
	}
	
	// Update is called once per frame
	void Update () {

        if (damageTaken)
        {
            damageTimer -= Time.deltaTime;
            damagecolour = DamagePanel.color;
            damageCurAlpha += alphachange ;
            damagecolour.a = damageCurAlpha;
            DamagePanel.color = damagecolour;  
            if (damageTimer <= 0)
            {
                
                damageTaken = false;
                damageTimer = MaxDamageTimer;
                damagecolour.a = 0;
                damageCurAlpha = 0;
                DamagePanel.color = damagecolour; 
            }
        }
        if (Health > MaxHealth)
        {
            Health = MaxHealth;
        }

        if (!InBounds)
        {
            colour = BoundsPanel.color;

            if (curAlpha <= 0 )
            {
                
                curAlpha = 0.1f;
                if (alphachange <0)
                    alphachange *= -1;
            }
            if (curAlpha >= MaxAlpha)
            {

                curAlpha = MaxAlpha;
                if (alphachange > 0)
                    alphachange *= -1;

            }


            curAlpha = colour.a;
            curAlpha += alphachange;



            
            
            colour.a = curAlpha;


            BoundsPanel.color = colour;
            BoundsDeathTimer -= Time.deltaTime;
            //if out of bounds timer decreaes.
            if (BoundsDeathTimer <= 0)
            {
                Health = 0;
            }
            
        }
        else
        {
            //if in bounds timer increaes.
            colour = BoundsPanel.color;
            colour.a = 0;
            BoundsPanel.color = colour;


            BoundsDeathTimer += Time.deltaTime;
            if (BoundsDeathTimer >= BoundsDeathTimerMax)
            {
                BoundsDeathTimer = BoundsDeathTimerMax;
            }
        }
        if (gameObject.GetComponent<ShipMovement>().CanControl == false)
        {
            EMPcolour = EMPPanel.color;

            if (EMPCurAlpha <= 0)
            {

                EMPCurAlpha = 0.1f;
                if (alphachange < 0)
                    alphachange *= -1;
            }
            if (EMPCurAlpha >= MaxAlpha)
            {

                EMPCurAlpha = MaxAlpha;
                if (alphachange > 0)
                    alphachange *= -1;

            }


            EMPCurAlpha = EMPcolour.a;
            EMPCurAlpha += alphachange;





            EMPcolour.a = EMPCurAlpha;


            EMPPanel.color = EMPcolour;
            
        }
        else
        {
            EMPcolour = EMPPanel.color;
            EMPcolour.a = 0;
            EMPPanel.color = EMPcolour;


        }


        if (Health <= 0)
        {
            //sets point position then creates point there.
            //gameObject.SetActive(false);
            gameObject.GetComponent<ShipMovement>().isDead = true;
            for (int i = 0; i < RespawnList.Length; i++)
            {
                if (RespawnList[i].GetComponent<PlayerSpawner>().Respawning != true)
                {
                    RespawnList[i].GetComponent<PlayerSpawner>().SetSpawner(gameObject);
                    //RespawnSet = true;
                    i = RespawnList.Length + 1;

                    
                    
                }



            }

            explosionFX.Play();
            if (!DiedToWall)
            {
            PointDrop.transform.position = transform.position;
            Instantiate(PointDrop);

            }
            else
            {
                DiedToWall = true;
            }
            // Instantiate(deathFX, gameObject.transform.position, Quaternion.identity);

            DiedToWall = false;

            //sets spawner's player to this.
            
            //while (!RespawnSet)
            
            
            //activates audio
            gameObject.GetComponent<ShipStats>().enabled = false;
            audioSourceDeath.Play();
            //deactivates script for respawning.
        }
        else
        {
            explosionFX.Stop();
        }

    }
    public void AddScore(int a_score)
    {
        Score += a_score;
    }
    public void HealthDamage(float a_damage)
    {
        Health -= a_damage;
        damageTaken = true;
        

    }
    public float ReturnHealth()
    {
        return Health;
    }
}
