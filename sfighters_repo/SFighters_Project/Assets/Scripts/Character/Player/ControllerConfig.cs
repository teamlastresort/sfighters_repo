﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerConfig : MonoBehaviour {
    public List<List<string[]>> Config = new List<List<string[]>>();
    public List<string[]> Controller = new List<string[]>();
    public string[] XMovement = new string[2];
    public string[] YMovement = new string[2];
    public string[] Throttle= new string[2];
    public string[] Boost = new string[2];
    public string[] BarrelRoll = new string[2];
    public string[] Shoot = new string[2];
    public string[] UsePower = new string[2];
    public string[] Rotate = new string[2];
    // Use this for initialization
    void Start () {

        
        XMovement[1] = "XMovement";
        YMovement[1] = "YMovement";
        Shoot[1] =     "Shoot";
        BarrelRoll[1] ="BarrelRoll";
        Boost[1] =     "Boost";
        UsePower[1] =  "UsePower";
        Throttle[1] =  "Throttle";
        Rotate[1] =    "Rotate";

        Controller.Add(XMovement);
        Controller.Add(YMovement);
        Controller.Add(Shoot);
        Controller.Add(BarrelRoll);

        Controller.Add(Boost);
        Controller.Add(UsePower);
        Controller.Add(Throttle);
        Controller.Add(Rotate);

        Config.Add(Controller);


    }
	
	// Update is called once per frame
    public string ReturnName(List<string[]> a_list, string a_String)
    {
        for (int i = 0; i < a_list.Count; i++)
        {
            if (a_list[i][1] == a_String)
            {
                return a_list[i][0];
            }
        }
        return "";
    }
}
