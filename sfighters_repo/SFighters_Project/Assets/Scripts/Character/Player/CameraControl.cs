﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public Transform cameraTarget;
    private Vector3 CameraDirection;
    private float X;
    private float Y;
    public float ZPos;
    public float YPos;
    public float MaxRotation;
    public float maxFOV;
    private float minFOV;
    public float FOVGrowthRate;
    public GameObject Parent;
    public Camera Cam;
    private int PlayerNum;
    // Use this for initialization
    void Start ()
    {
        PlayerNum = transform.parent.parent.GetComponent<LoadShipChoice>().ReturnPlayerNum();
        minFOV = 75;
        //cameraTarget = transform.parent.GetComponent<ShipMovement>().ChildMesh.transform;
        X = cameraTarget.parent.GetComponent<ShipMovement>().ReturnX();
        Y = cameraTarget.parent.GetComponent<ShipMovement>().ReturnY();
        CameraDirection = cameraTarget.position - transform.position;
        CameraDirection.Normalize();
        Quaternion targetRotation = Quaternion.LookRotation(CameraDirection);
        transform.rotation = targetRotation;
        transform.SetParent(Parent.transform);
        Cam.rect = gameObject.GetComponentInParent<LoadShipChoice>().CamRect;

    }

    // Update is called once per frame
    void LateUpdate ()
    {
        if (Parent.GetComponent<ShipMovement>().Boosted)
        {
            if (gameObject.GetComponent<Camera>().fieldOfView < maxFOV)
            {
                gameObject.GetComponent<Camera>().fieldOfView += FOVGrowthRate * Time.deltaTime;

                if (Mathf.Approximately(maxFOV, gameObject.GetComponent<Camera>().fieldOfView))
                    gameObject.GetComponent<Camera>().fieldOfView = maxFOV;
            }
            if (gameObject.GetComponent<Camera>().fieldOfView > maxFOV)
                gameObject.GetComponent<Camera>().fieldOfView = maxFOV;

        }
        else
        {
            if (gameObject.GetComponent<Camera>().fieldOfView > minFOV)
            {
                gameObject.GetComponent<Camera>().fieldOfView -= FOVGrowthRate * Time.deltaTime;


                if (Mathf.Approximately(minFOV, gameObject.GetComponent<Camera>().fieldOfView))
                    gameObject.GetComponent<Camera>().fieldOfView = minFOV;
            }

            if (gameObject.GetComponent<Camera>().fieldOfView < minFOV)
                gameObject.GetComponent<Camera>().fieldOfView = minFOV;
        }



        if(Vector3.Angle(transform.up, Vector3.up) <= 90.0f && Vector3.Angle(transform.forward, Vector3.up) >= 0.0f)
        {
            transform.localPosition = new Vector3(-X * 2, -Y + YPos,  ZPos);
        }
        else
        {
            transform.localPosition = new Vector3(-X * 2, Y + YPos, ZPos);
        }
        
        //transform.Rotate(new Vector3(-Y * MaxRotation, 0, -X * MaxRotation));

        
    }
    
}
