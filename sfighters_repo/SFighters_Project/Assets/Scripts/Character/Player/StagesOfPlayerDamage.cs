﻿using UnityEngine;
using System.Collections;

public class StagesOfPlayerDamage : MonoBehaviour {

    private float HealthStatus;
    private ShipStats shipHealth;

    public ParticleSystem stageOneDamage;
    public ParticleSystem stageTwoDamage;
    public ParticleSystem stageThreeDamage;


    // Use this for initialization
    void Start ()
    {
        shipHealth = GetComponent<ShipStats>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(shipHealth.Health >= 51 && shipHealth.Health <= 75)
        {
            stageOneDamage.Play();
        }   
        else
        {
            stageOneDamage.Stop();
        }

        if(shipHealth.Health >= 26 && shipHealth.Health <= 50)
        {
            stageTwoDamage.Play();
        }
        else
        {
            stageTwoDamage.Stop();
        }

        if(shipHealth.Health >= 0 && shipHealth.Health <= 25)
        {
            stageThreeDamage.Play();
        }
        else
        {
            stageThreeDamage.Stop();
        }
	}

}
