﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Button : MonoBehaviour {
    public bool isSelected;
    public bool StartSelected;
    public string SceneName;
    public Sprite Selected;
    public Sprite DeSelected;
    private Image rend;
    // Use this for initialization
    void Start () {
    rend = gameObject.GetComponent<Image>();
        if (StartSelected)
        {
            isSelected = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (isSelected)
        {
            rend.sprite = Selected;
        }
        else
        {
            rend.sprite = DeSelected;

        }
    }
    public string ReturnScene()
    {
        return SceneName;
    }
}
