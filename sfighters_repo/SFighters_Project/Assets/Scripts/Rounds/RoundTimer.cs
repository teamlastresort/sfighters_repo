﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class RoundTimer : MonoBehaviour
{

    public float timeRemaining;
    public float restartTimer;
    public float restartDelay = 3.0f;
    public Text text;

    string[] Maps = new string[6] { "Arena1_Prototype", "Arena2_Prototype", "Arena3_Prototype", "Arena4_prototype", "Arena5_Prototype", "Arena6_Prototype" };

	// Use this for initialization
	void Start ()
    {
        
        

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            timeRemaining = 3;
        }
        timeRemaining -= Time.deltaTime;
        
        string minutes = Mathf.Floor(timeRemaining / 60).ToString("00");
        string seconds = Mathf.Floor(timeRemaining % 60).ToString("00");
        
        text.text = minutes + ":" + seconds;
       

        if(timeRemaining <= 0)
        {
            GameOver();
            //SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex + 1 );
        }
	}

    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
