﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class WinningPlayer : MonoBehaviour {

    public Text[] ScoreText;
    public Text[] RankingText;
    private List<PlayerScorePair> playerScores = new List<PlayerScorePair>();

    // Use this for initialization
    void Start ()
    {
        //create a list of players in the game
        for( int i = 0; i < 4; i++ )
        {
            PlayerScorePair psp = new PlayerScorePair();
            psp.playerID = i+1;
            psp.score = PlayerPrefs.GetInt("P" + psp.playerID.ToString() + "Score", 0);
            playerScores.Add(psp);
        }

        //sort the players   
        playerScores.Sort(delegate (PlayerScorePair p1, PlayerScorePair p2) {
            return (p1.score <= p2.score) ? 1 : -1;
        });

        //iterate over players and set the textboxes
        for(int i = 0; i < 4; i++)
        {
            if (PlayerPrefs.GetInt("PlayerNum") == 0)
                ScoreText[i].text = "Player 1   " + playerScores[i].score.ToString();
            else
                ScoreText[i].text = "Player " + playerScores[i].playerID.ToString() + "   " + playerScores[i].score.ToString();

            //deactivate textboxes for players that don't exist
            if (i > PlayerPrefs.GetInt("PlayerNum"))
            {
                ScoreText[i].gameObject.SetActive(false);
                RankingText[i].gameObject.SetActive(false);
            }
        }


    }
    


}
