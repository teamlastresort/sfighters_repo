﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour {

    

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
    void Update ()
    {
        
    }

    public void Play()
    {
        SceneManager.LoadScene("Player_Select");
    }
    
    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void LoadMapSelect()
    {
        SceneManager.LoadScene("Map_Select");
    }

    public void LoadLevelOne()
    {
        SceneManager.LoadScene("Arena1_Prototype");
    }
    public void LoadLevelTwo()
    {
        SceneManager.LoadScene("Arena2_Prototype");
    }
    public void LoadLevelThree()
    {
        SceneManager.LoadScene("Arena3_Prototype");
    }
    public void LoadLevelFour()
    {
        SceneManager.LoadScene("Arena4_Prototype");
    }
    public void LoadLevelFive()
    {
        SceneManager.LoadScene("Arena5_Prototype");
    }
    public void LoadLevelSix()
    {
        SceneManager.LoadScene("Arena6_Prototype");
    }




    public void Quit()
    {
        Application.Quit();
    }
}
