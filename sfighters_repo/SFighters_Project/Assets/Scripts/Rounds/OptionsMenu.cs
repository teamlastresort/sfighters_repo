﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using InControl;
public class OptionsMenu : MonoBehaviour {
    public List<GameObject> ButtonList = new List<GameObject>();
    private int CurButton;
    private int MaxButtons;
    private bool isReleased = true;

    private bool RFirstStick = false;
    private bool RStickHeld = false;
    private bool UFirstStick = false;
    private bool UStickHeld = false;
    public float holdChangetimer;
    /// <summary>
    /// time between holding the stick for quick scroll through and first knock of stick
    /// </summary>
    public float ScrollTimer;
    /// <summary>
    /// scroll through timer, buttons per second
    /// </summary>
    private float holdTimer;
    
    // Use this for initialization
    void Start () {
        
        //ButtonList[0].isSelected = true;
        MaxButtons = ButtonList.Count - 1;
        for (int i = 0; i < ButtonList.Count; i++)
        {
            if (ButtonList[i].GetComponent<Button>().StartSelected)
            {
                CurButton = i;
                ButtonList[i].GetComponent<Button>().isSelected = true;
            }
        }
	}

    // Update is called once per frame

    void Update()
    {

        var inputDevice = (InputManager.Devices.Count > 0) ? InputManager.Devices[0] : null;
        if (inputDevice == null)
        {
            // If no controller exists for this cube, just make it translucent.
        }
        else
        {
            UpdateScroll(inputDevice);
        }
    }
    void UpdateScroll(InputDevice inputDevice) {


        if (inputDevice.DPadDown  && isReleased == true)
        {
            ButtonList[CurButton].GetComponent<Button>().isSelected = false;
            CurButton++;
            isReleased = false;
            if (CurButton > MaxButtons)
            {
                CurButton = 0;
            }

        }
        if (inputDevice.DPadRight && isReleased == true)
        {
            ButtonList[CurButton].GetComponent<Button>().isSelected = false;

            CurButton--;
            isReleased = false;
            if (CurButton < 0)
            {
                CurButton = MaxButtons;
            }

        }
        if (!inputDevice.DPadDown && isReleased == true)
            isReleased = true;


        if (inputDevice.Action1 != 0)
        {
            if (ButtonList[CurButton].GetComponent<Button>().SceneName != "Quit")
                SceneManager.LoadScene(ButtonList[CurButton].GetComponent<Button>().SceneName);
            else
                Application.Quit();

        }

        if (inputDevice.LeftStick.Left.Value > .85 || inputDevice.LeftStick.Up.Value > .85)
        {
            if (!UFirstStick)
            {
                UFirstStick = true;
                if (!RFirstStick)
                {
                    holdTimer = holdChangetimer;
                    ButtonList[CurButton].GetComponent<Button>().isSelected = false;
                    CurButton--;
                }
            }
            holdTimer -= Time.deltaTime;

            if (holdTimer <= 0)
            {

                ButtonList[CurButton].GetComponent<Button>().isSelected = false;

                CurButton--;
                if (UFirstStick == true)
                {
                    UStickHeld = true;
                }
                if (UStickHeld == true && holdTimer <= 0)
                {
                    holdTimer = ScrollTimer;
                }
            }

                if (CurButton < 0)
                {
                    CurButton = MaxButtons;
                }
        }
        else
        {
            UFirstStick = false;
            UStickHeld = false;

        }
        if (inputDevice.LeftStick.Right.Value > .85 || inputDevice.LeftStick.Down.Value > .85)
        {
            ButtonList[CurButton].GetComponent<Button>().isSelected = false;
            if (!RFirstStick)
            {
                RFirstStick = true;
                if (!UFirstStick)
                {
                    holdTimer = holdChangetimer;
                    ButtonList[CurButton].GetComponent<Button>().isSelected = false;
                    CurButton++;
                }
            }
            holdTimer -= Time.deltaTime;

            if (holdTimer <= 0)
            {
                CurButton++;
                isReleased = false;
                if (RFirstStick == true)
                {
                    RStickHeld = true;
                }
                if (RStickHeld == true && holdTimer <= 0)
                {
                    holdTimer = ScrollTimer;
                }
            }
                if (CurButton > MaxButtons)
                {
                    CurButton = 0;
                }
        }
        else
        {
            RFirstStick = false;
            RStickHeld = false;

        }
        ButtonList[CurButton].GetComponent<Button>().isSelected = true;


    }
}
