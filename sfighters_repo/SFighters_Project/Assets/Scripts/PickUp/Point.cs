﻿using UnityEngine;
using System.Collections;

public class Point : MonoBehaviour {

    private AudioManager audioManager;
    public AudioSource audioSourcePoint;
    public float PointVolume;
    public int PointValue;
    public float HealthReturn;

    //FX Particle
    public GameObject crystalParticles;
    // Use this for initialization
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!(other.GetComponentInParent<ShipMovement>().isDead))
            {
            audioSourcePoint.PlayOneShot(audioManager.PointPickup, PointVolume);
            other.GetComponentInParent<ShipStats>().AddScore(PointValue);
            other.GetComponentInParent<ShipStats>().HealthDamage(-HealthReturn);
            Destroy(gameObject);
            Instantiate(crystalParticles, other.gameObject.transform.position,Quaternion.identity);
            }
        }
    }
}
