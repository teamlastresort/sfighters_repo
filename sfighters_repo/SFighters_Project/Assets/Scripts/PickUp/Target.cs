﻿using UnityEngine;
using System.Collections;

public class Target : PickUp
{



    // Use this for initialization
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void WasHit()
    {
        audioSourcePickUp.PlayOneShot(audioManager.PowerPickup, PickUpVolume);


        Spawner.GetComponent<PickUpSpawner>().StartRespawn();
        gameObject.SetActive(false);

    }

    protected override void OnTriggerEnter(Collider other) 
    {
 
    }


}

