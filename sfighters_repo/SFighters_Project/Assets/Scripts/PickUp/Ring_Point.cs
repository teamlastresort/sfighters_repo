﻿using UnityEngine;
using System.Collections;

public class Ring_Point : MonoBehaviour {
    public GameObject RingSpawner;
    public int HealthIncrease;

    [Tooltip("This is the Ring Pickup FX Prefab")]
    public GameObject ringPickup;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.GetComponentInParent<ShipStats>().Health += HealthIncrease;
            RingSpawner.GetComponent<RingSpawner>().StartRespawn();
            Instantiate(ringPickup, gameObject.transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }
}
