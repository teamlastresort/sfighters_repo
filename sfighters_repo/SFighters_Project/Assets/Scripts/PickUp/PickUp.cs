﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {
    public int PickUpNum;
    public GameObject Spawner;
    protected AudioManager audioManager;
    public AudioSource audioSourcePickUp;
    public float PickUpVolume;

    [Tooltip("This is the PowerUp FX Prefab")]
    public GameObject powerPickUp;


    // Use this for initialization
    void Start () {
    audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            audioSourcePickUp.PlayOneShot(audioManager.PowerPickup, PickUpVolume);
            PickUpNum = Random.Range(0, 100);
            PickUpNum %= 3;
            other.GetComponentInParent<Inventory>().AddItem(PickUpNum);
            Spawner.GetComponent<PickUpSpawner>().StartRespawn();
            Instantiate(powerPickUp, gameObject.transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }


}
