﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using InControl;
using System;

public class RevampedInput : MonoBehaviour {

    public GameObject[] shipModels = new GameObject[4];

    private InputDevice[] inputDevice = new InputDevice[4];

    public GameObject[] uiPanels = new GameObject[4];

   
    private int[] shipIndex = { 0, 0, 0, 0 };

    public GameObject[] Ship = new GameObject[4];


    private int[] PlayerChoice = { 1, 2, 3, 4 };

    private bool[] hasSelected = { false, false, false, false };
    [HideInInspector]
    public bool[] selectingShip = { false, false, false, false };
    public GameObject[] aButton = new GameObject[4];


    public GameObject StartText;
    public static int playerNum = 0;
    private int TotalSelected;

    private bool[] firstLogin = { true, true, true, true };

    private string PlayerName;

    // Use this for initialization
    void Start()
    {
        TotalSelected = 0;
        StartText.gameObject.SetActive(false);
        

        for (int i = 0; i < 4; i++)
        {
            
           Ship[i].SetActive(false);
            

            uiPanels[i].gameObject.SetActive(false);

        }

    }

    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < 4; i++)
        {
            inputDevice[i] = (InputManager.Devices.Count > i) ? InputManager.Devices[i] : null;
        }
        playerNum = InputManager.Devices.Count;

        for (int i = 0; i < playerNum; i++)
        {
            if (inputDevice[i] != null)
            {
                inputDevice[i].Vibrate(1.0f, 2.0f);
                UpdateSelection(inputDevice[i], i);

            }
        }
    }

    void UpdateSelection(InputDevice inputDevice, int PNum)
    {
        TotalSelected = 0;
        PlayerName = "P" + (PNum + 1) + "Choice";

        if (firstLogin[PNum] == true &&inputDevice.Action1.WasPressed)
        {
            Ship[PNum].SetActive(true);
            uiPanels[PNum].gameObject.SetActive(true);

            aButton[PNum].gameObject.SetActive(false);
            selectingShip[PNum] = true;
            firstLogin[PNum] = false;
        }


        if (inputDevice.Action3.WasPressed)
        {
            if (hasSelected[PNum] == false)
            {
                uiPanels[PNum].gameObject.SetActive(false);
                PlayerChoice[PNum] = shipIndex[PNum];

                PlayerPrefs.SetInt(PlayerName, PlayerChoice[PNum]);
                hasSelected[PNum] = true;
                selectingShip[PNum] = false;
            }
            else
            {
                uiPanels[PNum].gameObject.SetActive(true);
                PlayerChoice[PNum] = 0;
                PlayerPrefs.SetInt(PlayerName, PlayerChoice[PNum]);
                hasSelected[PNum] = false;
                selectingShip[PNum] = true;
            }

        }




        TotalSelected = -1;
        for (int i = 0; i < hasSelected.Length; i++)
        {
            if (hasSelected[i])
            {
                TotalSelected++;

            }

        }

        if (TotalSelected >= 0 && inputDevice.CommandWasPressed)
        {
            PlayerPrefs.SetInt("PlayerNum", TotalSelected);
            //Debug.Log(PlayerPrefs.GetInt("PlayerNum"));
            GetComponent<SwitchScene>().LoadMapSelect();
        }
        if (TotalSelected >= 0)
        {
            StartText.SetActive(true);
        }
        else
        {
            StartText.SetActive(false);
        }

    }


    private GameObject SwapShipMesh(GameObject originalShip, GameObject newShip)
    {
        //Vector3 pos = originalShip.transform.position;
        //Quaternion rot = originalShip.transform.rotation;
        GameObject ship = (GameObject)Instantiate(newShip, originalShip.transform.parent);
        ship.transform.localPosition = Vector3.zero;
        ship.transform.localRotation = Quaternion.identity;

        Destroy(originalShip);
        return ship;
    }

    /// <summary>
    /// Update index and swap ship
    /// </summary>
    /// <param name="playerIndex"></param>
    /// <param name="curButton"></param>
    public void UpdateIndex(int playerIndex, int curButton)
    {
        if (hasSelected[playerIndex] == true || !selectingShip[playerIndex])
            return;

        shipIndex[playerIndex] = curButton;

        Ship[playerIndex] = SwapShipMesh(Ship[playerIndex], shipModels[shipIndex[playerIndex]]);

        Ship[playerIndex].transform.localScale = Vector3.one * 1.25f;

    }
}
