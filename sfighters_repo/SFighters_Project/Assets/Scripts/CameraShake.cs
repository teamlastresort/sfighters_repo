﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

    //[HideInInspector]
    public Camera playerCamera;


    private Animator animator;

	// Use this for initialization
	void Start ()
    {
   
        playerCamera = GetComponentInChildren<Camera>();
        animator = playerCamera.GetComponent<Animator>();

    }
	
	


    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            animator.SetTrigger("IsShaking");
        }
        if (other.gameObject.tag == "Collidables")
        {
            animator.SetTrigger("IsShaking");
        }
    }


}
