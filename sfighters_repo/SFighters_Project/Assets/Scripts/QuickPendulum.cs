﻿//Quick Scripts by Jack Wilson, Wanderlight Games 2017.
//Thank you for purchasing this product.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu ("Quick Scripts/Quick Pendulum")]
public class QuickPendulum : MonoBehaviour {

	public float angle = 90;
	public float speed = 1;

	void FixedUpdate () {
		float x = (Mathf.Sin (Time.fixedTime * speed));
		transform.rotation = Quaternion.Euler (Vector3.right * angle * x);
	}
}
