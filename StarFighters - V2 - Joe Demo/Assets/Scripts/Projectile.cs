﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    private Vector3 bulletVelocity;
    public GameObject hitParticles;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
       //moves bullet
        transform.position += bulletVelocity * Time.deltaTime;
	}


    public void ShootProjectile(Vector3 direction, float speed)
    {
        //assigns bullet velocity.
        bulletVelocity = direction * speed;
    }

    public void OnCollisionEnter(Collision bulletCollision)
    {
        //destroys bullet on collision
        Destroy(gameObject);

        //get the particle system from the hitParticles object
        ParticleSystem particleSystem = hitParticles.GetComponent<ParticleSystem>();

        //set up vector based on collision normal
        particleSystem.transform.up = bulletCollision.contacts[0].normal;

        //play the particle system
        particleSystem.Play();

        //set the particle parent to null
        hitParticles.transform.SetParent(null);

        //set the particles to destroy 0.5f seconds after.
        Destroy(hitParticles, particleSystem.duration + 0.5f);
    }


}
