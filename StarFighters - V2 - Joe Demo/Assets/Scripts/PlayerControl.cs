﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public float PlayerMaxForwardMovement = 10.0f;
    public float PlayerRotateSpeed = 10.0f;
    public float ProjectileSpeed = 15.0f;
    public float PlayerForwardMovement = 0.0f;
    public float PlayerVerticleMovement = 0.0f;

    private float horizontalAxis = 0.0f;
    private float verticalAxis = 0.0f;

    public float maxXRot = 90.0f;
    public float minXRot = -90.0f;
    

    public CharacterController charController;
    public GameObject bulletPrefab;
    public GameObject minePrefab;

    public bool CanControl;
    public float DisableTime;
    // Use this for initialization
    void Start ()
    {
        CanControl = true;

        charController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerMovement();
        if (CanControl == false)
        {
            DisableTime -= Time.deltaTime;
            if (DisableTime <= 0.0f)
            {
                UnlockControls();
            }
        }

	}


    public void LockControls()
    {
        if (CanControl == false)
            return;

        CanControl = false;
        DisableTime = 0.0f;
    }


    void DropEMP()
    {
        if (HasLandmines() == false)
            return;

        GameObject empMine = Instantiate(minePrefab, transform.position + transform.forward * -1.0f, Quaternion.identity) as GameObject;

        //call dropProjectile
        Emp_Mine mineScript = empMine.AddComponent<Emp_Mine>();

        if (mineScript == null)
            empMine.AddComponent<Emp_Mine>();

        //decrement landmines by 1


    }



    private void PlayerMovement()
    {


 

 

       
        transform.rotation = Quaternion.Euler(transform.right * verticalAxis * PlayerVerticleMovement * Time.deltaTime) * transform.rotation;


        Vector3 currentMovement = Vector3.zero;

        PlayerForwardMovement = PlayerForwardMovement + 5 * Time.deltaTime;
        if (PlayerForwardMovement > 50)
        {
            PlayerForwardMovement = 50;
        }

        currentMovement += transform.forward * PlayerForwardMovement * Time.deltaTime;
        charController.Move(currentMovement);
    }


}
