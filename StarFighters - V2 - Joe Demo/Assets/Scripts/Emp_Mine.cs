﻿using UnityEngine;
using System.Collections;

public class Emp_Mine : MonoBehaviour {

    private bool IsActive;
    private bool IsReady;
    public float DropTimer;

	// Use this for initialization
	void Start ()
    {
        DropTimer = 1.0f;
        IsActive = false;
        IsReady = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (IsActive == true)
        {
            GetComponent<SphereCollider>().radius += 1.0f * Time.deltaTime;
        }
        if (IsReady == false)
        {
            //decrement timer
            DropTimer -= Time.deltaTime;
            if (DropTimer <= 0)
                IsReady = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (IsReady == false)
            return;
        if(other.gameObject.tag == "Player")
        {

            IsActive = true;
            Destroy(gameObject, 1.0f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //ADD IN PLAYER FILTER  
        Destroy(gameObject);
    }

    void OnTriggerStay(Collider other)
    {
        if(IsActive == true)
        {//change to timer
            if(other.gameObject.tag == "Player")
            {
                other.GetComponent<PlayerControl>().LockControls();
            }
        }
    }




}
