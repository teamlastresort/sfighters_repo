﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    private Vector3 bulletVelocity;
    public GameObject hitParticles;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position += bulletVelocity * Time.deltaTime;
	}


    public void ShootProjectile(Vector3 bulletDirection, float bulletSpeed)
    {
        bulletVelocity = bulletDirection * bulletSpeed;
    }

    public void OnCollisionEnter(Collision bulletCollision)
    {
        Destroy(gameObject);

        ParticleSystem particleSystem = hitParticles.GetComponent<ParticleSystem>();
        particleSystem.transform.up = bulletCollision.contacts[0].normal;

        particleSystem.Play();
        hitParticles.transform.SetParent(null);

        Destroy(hitParticles, particleSystem.duration + 0.5f);
    }


}
