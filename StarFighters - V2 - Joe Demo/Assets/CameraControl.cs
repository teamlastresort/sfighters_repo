﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public Transform cameraTarget;
    public float cameraDistance;
    public float cameraFollowSpeed = 10.0f;
    private Vector3 cameraTargetPosition;
    

	// Use this for initialization
	void Start ()
    {
        
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        cameraTargetPosition = cameraTarget.transform.position + cameraTarget.transform.forward * -cameraDistance;
        transform.position = Vector3.Lerp(transform.position, cameraTargetPosition, (Time.deltaTime * cameraFollowSpeed) );

        transform.LookAt(cameraTarget, Vector3.up);

        //if (cameraTargetPosition.x > 90)
        //{
        //    cameraTargetPosition = new Vector3(90, 0, 0);
        //}
        //else
        //{
        //    if (cameraTargetPosition.x < -90)
        //    {
        //        cameraTargetPosition = new Vector3(-90, 0, 0);
        //    }
        //}



        //if (cameraTarget.localEulerAngles.x > 180)
        //{
        //    cameraTarget.localEulerAngles = new Vector3(180, 0, 0);
        //}
        //else
        //{
        //    if (cameraTarget.localEulerAngles.x < -180)
        //    {
        //        cameraTarget.localEulerAngles = new Vector3(-180, 0, 0);
        //    }
        //}


    }
}
