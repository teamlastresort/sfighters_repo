﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public float PlayerMaxForwardMovement = 10.0f;
    public float PlayerRotateSpeed = 10.0f;
    public float ProjectileSpeed = 15.0f;
    public float PlayerForwardMovement = 0.0f;
    public float PlayerVerticleMovement = 0.0f;

    private float horizontalAxis = 0.0f;
    private float verticalAxis = 0.0f;

    public float maxXRot = 90.0f;
    public float minXRot = -90.0f;
    

    public CharacterController charController;
    public GameObject bulletPrefab;

    // Use this for initialization
    void Start ()
    {
        charController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        if(Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

        horizontalAxis = Input.GetAxis("Horizontal");
        verticalAxis = Input.GetAxis("Vertical");
        //rotation on the y axis

        transform.rotation = Quaternion.Euler(new Vector3(0.0f, horizontalAxis * PlayerRotateSpeed * Time.deltaTime, 0.0f)) * transform.rotation;
        //    transform.rotation = Quaternion.Euler(Mathf.Clamp(45, minXRot, maxXRot));


        //rotation on the x axis
        //if rotationX < 90 and you are pressing down

        float angle = Vector3.Angle(Vector3.up, transform.forward);

        if (angle < 20.0f && verticalAxis < 0.0f)
        {
            verticalAxis = 0.0f;
        }

        if (angle > 160.0f && verticalAxis > 0.0f)
        {
            verticalAxis = 0.0f;
        }

        transform.rotation = Quaternion.Euler(transform.right * verticalAxis * PlayerVerticleMovement * Time.deltaTime) * transform.rotation;



  
        Vector3 currentMovement = Vector3.zero;

        //Increases player speed incrementally until maxed
        PlayerForwardMovement = PlayerForwardMovement + 5 * Time.deltaTime;
        if(PlayerForwardMovement > 50)
        {
            PlayerForwardMovement = 50;
        }

        currentMovement += transform.forward * PlayerForwardMovement * Time.deltaTime;
        charController.Move(currentMovement);
	}


    void Shoot()
    {
        //instantiate projectile
        GameObject projectile = Instantiate(bulletPrefab, transform.position + transform.forward * 2.0f, Quaternion.identity) as GameObject;
        GameObject.Destroy(projectile, 5.0f);
        //call ShootProjectile
        Projectile projectileScript = projectile.GetComponent<Projectile>();

        if (projectileScript == null)
            projectileScript = projectile.AddComponent<Projectile>();

        projectileScript.ShootProjectile(transform.forward, ProjectileSpeed);
    }
}
